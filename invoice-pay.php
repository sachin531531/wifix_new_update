<?php 
    include 'db/dbConnection.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>WEFIX</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <link rel="icon" href="assets/img/icon.ico" type="image/x-icon" />

    <!-- Fonts and icons -->
    <script src="assets/js/plugin/webfont/webfont.min.js"></script>
    <script>
    WebFont.load({
        google: {
            "families": ["Lato:300,400,700,900"]
        },
        custom: {
            "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands",
                "simple-line-icons"
            ],
            urls: ['assets/css/fonts.min.css']
        },
        active: function() {
            sessionStorage.fonts = true;
        }
    });
    </script>

    <!--   Core JS Files   -->
    <script src="assets/js/core/jquery.3.2.1.min.js"></script>
    <!-- CSS Files -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/atlantis.min.css">
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link rel="stylesheet" href="assets/css/demo.css">
</head>

<body>
    <div class="wrapper">
        <!-- Navbar Header -->
        <?php include('header.php');?>
        <!-- End Navbar -->
        <!-- Sidebar -->
        <?php include('sidebar.php');?>
        <!-- End Sidebar -->
        <div class="main-panel">
            <div class="content">
                <?php

                    $date = date("m/d/Y");
                    $txtID =  $_POST['txtID'];
                    $txtTotal =  $_POST['txtTotal'];
                    $txtPayment =  $_POST['txtPayment'];
                    $user = $_SESSION['user_id'];

                    $totalPay = 0.0;
                    $sql4="SELECT * FROM payment_tbl WHERE pay_inv = $txtID;";
                    $result4 = mysqli_query($connection,$sql4);
                    while($dataRow4=mysqli_fetch_assoc($result4)){ 
                        $totalPay += $dataRow4['pay_price'];
                    }
                    
                    $totalPay += $txtPayment;

                    $sql = "INSERT INTO payment_tbl(`pay_id`,`pay_inv`,`pay_price`,`pay_date`,`pay_user`) VALUES (0,$txtID,$txtPayment,'$date',$user)"; ?>

                <?php  if ($connection->query($sql)===true){

                    if ($totalPay == $txtTotal) {
                       $sql1 = "UPDATE invoice_tbl SET 
                                        payment = '1'
                                        WHERE 
                                        invoice_id=$txtID";
                    if ($connection->query($sql1)===true){

                        $sql2 = mysqli_query($connection, "SELECT * FROM invoice_tbl WHERE invoice_id=$txtID");
                        $res = mysqli_fetch_array($sql2);
                        $job_no = $res['invoice_job'];

                        $sql3 = "UPDATE job_tbl SET 
                                        job_status = 'Closed'
                                        WHERE 
                                        job_id='$job_no'";
                        if ($connection->query($sql3)===true){}
                    }
                    } 
                    
                    ?>
                <script>
                var SweetAlert2Demo = function() { 

                    var initDemos = function() {
                        swal({
                            icon: "success",
                            title: 'Success !',
                            type: 'success',
                            buttons: {
                                confirm: {
                                    text: 'OK',
                                    className: 'btn btn-success'
                                }
                            }
                        }).then((Delete) => {
                            if (Delete) {
                                window.location.replace("list-invoice.php");
                            } else {
                                window.location.replace("list-invoice.php");
                            }
                        });
                    };

                    return {
                        init: function() {
                            initDemos();
                        },
                    };
                }();

                jQuery(document).ready(function() {
                    SweetAlert2Demo.init();
                });
                </script>
                <?php }else{ ?>
                <script>
                var SweetAlert2Demo = function() {

                    var initDemos = function() {
                        swal({
                            icon: "error",
                            title: 'Not Success !',
                            type: 'error',
                            buttons: {
                                confirm: {
                                    text: 'OK',
                                    className: 'btn btn-danger'
                                }
                            }
                        }).then((Delete) => {
                            if (Delete) {
                                window.location.replace("list-invoice.php");
                            } else {
                                window.location.replace("list-invoice.php");
                            }
                        });
                    };

                    return {
                        init: function() {
                            initDemos();
                        },
                    };
                }();

                jQuery(document).ready(function() {
                    SweetAlert2Demo.init();
                });

                // window.location.replace("list-customers.php");
                </script>
                <?php } ?>
            </div>
            <!-- footer -->
            <?php include('footer.php');?>
            <!-- End footer -->
        </div>

        <!-- Custom template | don't include it in your project! -->
        <?php include('rightSidebar.php');?>
        <!-- End Custom template -->
    </div>

    <script src="assets/js/core/popper.min.js"></script>
    <script src="assets/js/core/bootstrap.min.js"></script>
    <!-- jQuery UI -->
    <script src="assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <script src="assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

    <!-- jQuery Scrollbar -->
    <script src="assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <!-- Atlantis JS -->
    <script src="assets/js/atlantis.min.js"></script>
    <!-- Atlantis DEMO methods, don't include it in your project! -->
    <script src="assets/js/setting-demo2.js"></script>
    <!-- Sweet Alert -->
    <script src="assets/js/plugin/sweetalert/sweetalert.min.js"></script>
</body>

</html>