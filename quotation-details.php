<!DOCTYPE html>
<html lang="en" id="printElement">

<?php
session_start();

    if (!isset($_SESSION['user_name'])) {
        header('Location: login.php?err=1');
    }
?>

<?php include 'db/dbConnection.php'; ?>

<?php
$id = $_GET['id'];
?>

<head>
    <meta charset="utf-8">
    <title>Quotation | WEFIX</title>
    <link rel="icon" href="assets/img/icon.ico" type="image/x-icon" />
    <script src="assets/js/plugin/webfont/webfont.min.js"></script>
    <script>
    WebFont.load({
        google: {
            "families": ["Lato:300,400,700,900"]
        },
        custom: {
            "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands",
                "simple-line-icons"
            ],
            urls: ['assets/css/fonts.min.css']
        },
        active: function() {
            sessionStorage.fonts = true;
        }
    });
    </script>
    <link rel="stylesheet" href="assets/css/invoiceStyle.css" media="all" />
    <link rel="stylesheet" href="assets/css/atlantis.min.css">
    <style>
    /* override styles when printing */
    @media print {
        #openWin {
            display: none;
        }

        #backbtn {
            display: none;
        }

        .tox-menubar{
            display: none !important;
        }
        .tox-toolbar{
            display: none !important;
        }
        .tox-statusbar{
            display: none !important;
        }
        #tinymce{
            overflow-y: hidden !important;
        }
        .tox{
            border: none !important;
        }
        .tox-tinymce{
            border: none !important;
        }
        .tox-edit-area{
            border: none !important;
        }
    }
    .tox-statusbar__branding{
        display: none !important;
    }
    </style>
</head>

<button type="button" id="openWin" class="btn btn-icon btn-round" title=""
    style="position:fixed;margin:auto; bottom:250px; right:10px; width:60px; height:60px; background-color:#5C55BF; border:none; cursor:pointer;"
    onclick="window.print();return false;">
    <i class="fas fa-print" style="font-size:180%; color:white;"></i>
</button>
<button type="button" class="btn btn-icon btn-round" id="backbtn"
    style="position:fixed;margin:auto; bottom:160px; right:10px; width:60px; height:60px; background-color:#5C55BF; border:none; cursor:pointer;"
    onclick="goBack()">
    <i class="fas fa-arrow-left" style="font-size:200%; color:white;"></i>
</button>

<body  style="background-color: white;">

    <?php
    $total = 0;

    $sql = mysqli_query($connection, "SELECT * FROM quatation_tbl WHERE  quate_id = '$id'");
    $res = mysqli_fetch_array($sql);
        var_dump($id);
        $added_user = $res['added_user'];
        $added_date = $res['added_date'];
        $added_time = $res['added_time'];
        $disc = $res['quote_disc'];
        $advance = $res['quote_advance'];
        $quote_no = $res['quote_no'];
        $addTax = $res['add_tax'];
        $cusId = $res['quote_cusID'];

        $tranpotation_cost = $res['transporation_cost'];
        $over_head_cost = $res['over_head_cost'];
        $profit_margines = $res['profit_margines'];
        $conditions = $res['conditions'];

        




    $sql = mysqli_query($connection, "SELECT * FROM customer_tbl WHERE cus_id = '$cusId'");
    $res = mysqli_fetch_array($sql);

        $customer = $res['cus_name'];
        $custAddress = $res['address'];
        $custEmail = $res['cus_email'];

    $x = 1;
    ?>

    <?php
    $sql1 = mysqli_query($connection, 'SELECT * FROM invo_quotesetting_tbl WHERE id = 1');
    $res1 = mysqli_fetch_array($sql1);

    $name = $res1['name'];
    $number = $res1['number'];
    $email = $res1['email'];
    $image = $res1['image'];
    $address = $res1['address'];
    $footer = $res1['footer'];
    ?>

    <header class="clearfix"  style="background-color: white;">
        <div id="logo">
            <img src="image/logo21.jpg">
        </div>
        <div id="company">
            <h2 class="name"><?php echo $name; ?></h2>
            <div><?php echo $address; ?></div>
            <div><?php echo $number; ?></div>
            <div><a href="mailto:<?php echo $email; ?>" target="_new"><?php echo $email; ?></a></div>
        </div>
        </div>
    </header>
    <main style="background-color: white;">
        <div id="details" class="clearfix">
            <div id="client">
                <div class="to">QUOTE TO:</div>
                <h2 class="name"><?php echo $customer; ?></h2>
                <div class="address"><?php echo $custAddress; ?></div>
                <div class="email"><a href="mailto:<?php echo $custEmail; ?>" target="_new"><?php echo $custEmail; ?></a></div>
            </div>
            <div id="invoice">
                <h1>QUOTATION</h1>
                <div class="date">Date : <?php echo $added_date; ?></div>
                <div class="date">Quote Number : <?php echo $quote_no; ?></div>
                <!-- <div class="date">Valid Date: 30/06/2014</div> -->
            </div>
        </div>
        <table border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th class="unit" style="border-bottom: 0.5px solid #AAAAAA;">S.NO</th>
                    <th class="desc" style="border-bottom: 0.5px solid #AAAAAA;">DESCRIPTION</th>
                    <th class="unit" style="border-bottom: 0.5px solid #AAAAAA;">UNIT</th>
                    <th class="qty" style="border-bottom: 0.5px solid #AAAAAA;">QTY</th>
                    <th class="unit" style="border-bottom: 0.5px solid #AAAAAA;">UNIT PRICE (Rs)</th>
                    <th class="total" style="border-bottom: 0.5px solid #AAAAAA;">AMOUNT (Rs)</th>
                </tr>
            </thead>
            <tbody>

                <?php
                    $sql = "SELECT * From quate_details_tbl,stock_tbl,products_tbl WHERE quate_id = '$id' AND stock_tbl.stock_id = quate_details_tbl.stock_id AND products_tbl.pro_id = stock_tbl.pro_id";
                    $result = mysqli_query($connection, $sql);
                    while ($dataRow = mysqli_fetch_assoc($result)) {
                        $total += $dataRow['totQty'] * $dataRow['quote_price']; ?>

                <tr>
                    <td class="no"><?php echo $x; ?></td>
                    <td class="desc">
                        <h3><?php echo $dataRow['pro_name']; ?></h3>
                        <?php echo $dataRow['quate_desc']; ?>
                    </td>
                    <td class="unit"> <?php echo $dataRow['pro_unit']; ?> </td>
                    <td class="qty"><?php echo $dataRow['totQty']; ?></td>
                    <td class="unitPrice"><?php echo number_format($dataRow['quote_price'], 2); ?> </td>
                    <td class="total"><?php echo number_format($dataRow['totQty'] * $dataRow['quote_price'], 2); ?>
                    </td>
                </tr>
                <?php
                    ++$x;
                    }
                ?>

                <?php
                    $sql = "SELECT * FROM quate_lapack_tbl,labourpack_tbl WHERE quate_lapack_tbl.quateRef_id = '$id' AND labourpack_tbl.labourPack_id = quate_lapack_tbl.quitPack_id;";
                    $result = mysqli_query($connection, $sql);
                    while ($dataRow = mysqli_fetch_assoc($result)) {
                        $total += $dataRow['labourPack_price']; ?>

                <tr>
                    <td class="no"><?php echo $x; ?></td>
                    <td class="desc">
                        <h3><?php echo $dataRow['labourPack_name']; ?></h3>
                    </td>
                    <td class="unit">-</td>
                    <td class="qty">-</td>
                    <td class="unitPrice"><?php echo number_format($dataRow['labourPack_price'], 2); ?> </td>
                    <td class="total"><?php echo number_format($dataRow['labourPack_price'], 2); ?> </td>
                </tr>
                <?php
                    ++$x;
                    }
                ?>

                <?php
                    $sql = "SELECT * From quote_additional_tbl WHERE quote_id = '$id';";
                    $result = mysqli_query($connection, $sql);
                    while ($dataRow = mysqli_fetch_assoc($result)) {
                        $total += $dataRow['additional_price']; ?>

                <tr>
                    <td class="no"><?php echo $x; ?></td>
                    <td class="desc">
                        <h3><?php echo $dataRow['mainDec']; ?></h3>
                        <?php echo $dataRow['subDesc']; ?>
                    </td>
                    <td class="unit">-</td>
                    <td class="qty">-</td>
                    <td class="unitPrice"><?php echo number_format($dataRow['additional_price'], 2); ?> </td>
                    <td class="total"><?php echo number_format($dataRow['additional_price'], 2); ?> </td>
                </tr>
                <?php
                    ++$x;
                    }
                ?>

            </tbody>

            <?php
            $totWithDis = 0;
            $totDisc = 0;
            $totTax = 0;
            $totWithTax = 0;
            // $tranpotation_cost = 0; 
            // $over_head_cost = 0;
            // $profit_margines = 0;

            $sql = mysqli_query($connection, "SELECT * FROM tax_tbl WHERE id = '1'");
            $res = mysqli_fetch_array($sql);
            $tax = $res['tax'];

            $totDisc += ($total * $disc / 100);
            $totWithDis += $total - $totDisc;

            if ($addTax == 0) {
                $totTax += ($totWithDis * $tax / 100);
                $totWithTax += $totWithDis + $totTax + $tranpotation_cost + $over_head_cost + $profit_margines;
            }

            ?>

            <tfoot>
                <tr>
                    <td colspan="1"></td>
                    <td colspan="3"></td>
                    <td colspan="1" style="text-align:left;">GRAND TOTAL (Rs.) </td>
                    <?php if ($addTax == 0) {
                ?>
                        <td style="text-align:right;"><?php echo number_format($totWithTax, 2); ?>
                        </td>
                    <?php
            } else {
                ?>
                        <td style="text-align:right;"><?php echo number_format($totWithDis, 2); ?>
                        </td>
                    <?php
            } ?>
                    
                </tr>
            </tfoot>
            
            <tfoot>
                <br>
                <tr>
                    <td colspan="1"></td>
                    <td colspan="3"></td>
                    <td colspan="1" style="border-top: 0px solid #AAAAAA;text-align:left;">Transportation Cost</td>
                    <td style="border-top: 0px solid #AAAAAA; text-align:right;"><?php echo number_format($tranpotation_cost, 2); ?>
                    </td>
                </tr>
            </tfoot>

            <tfoot>
                <tr>
                    <td colspan="1"></td>
                    <td colspan="3"></td>
                    <td colspan="1" style="border-top: 0px solid #AAAAAA;text-align:left;">Overhead Cost</td>
                    <td style="border-top: 0px solid #AAAAAA; text-align:right;"><?php echo number_format($over_head_cost, 2); ?>
                    </td>
                </tr>
            </tfoot>
            
            <tfoot>
                <tr>
                    <td colspan="1"></td>
                    <td colspan="3"></td>
                    <td colspan="1" style="border-top: 0px solid #AAAAAA;text-align:left;">Profit Margines</td>
                    <td style="border-top: 0px solid #AAAAAA; text-align:right;"><?php echo number_format($profit_margines, 2); ?>
                    </td>
                </tr>
            </tfoot>
            <tfoot>
                <tr>
                    <td colspan="1"></td>
                    <td colspan="3"></td>
                    <td colspan="1" style="border-top: 0px solid #AAAAAA;text-align:left;">Discount <?php echo $disc; ?>%</td>
                    <td style="border-top: 0px solid #AAAAAA; text-align:right;"><?php echo number_format($totDisc, 2); ?>
                    </td>
                </tr>
            </tfoot>

           <?php if ($addTax == 0) {
                ?>
            <tfoot>
                <tr>
                    <td colspan="1"></td>
                    <td colspan="3"></td>
                    <td colspan="1" style="text-align:left;">Tax <?php echo $tax; ?>%</td>
                    <td style="text-align:right;"><?php echo number_format($totTax, 2); ?>
                    </td>
                </tr>
            </tfoot>
           <?php
            } ?>
        </table>
       <h2>Condition : <?php echo $conditions?></h2>
        <div id="thanks">
            <textarea class="form-control"><?php echo $footer; ?></textarea>
        </div>
    </main>

    <!--   Core JS Files   -->
    <script src="assets/js/core/jquery.3.2.1.min.js"></script>
    <script>
    function goBack() {
        window.history.back();
    }
    </script>

    <!-- partial -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.5/tinymce.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script>
    <script>
    tinymce.init({
        selector: "textarea",
        plugins: "lists advlist autolink autoresize charmap code emoticons hr image insertdatetime link media paste preview searchreplace table textpattern toc visualblocks visualchars wordcount quickbars autoresize spellchecker",
        toolbar: "undo redo | formatselect | fontselect | fontsizeselect | bold italic underline strikethrough backcolor forecolor | subscript superscript | numlist bullist | alignleft aligncenter alignright alignjustify | outdent indent | paste searchreplace | toc link image media charmap insertdatetime emoticons hr | table tabledelete | tableprops tablerowprops tablecellprops | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | removeformat spellchecker",
        insertdatetime_element: true,
        media_scripts: [{
                filter: 'platform.twitter.com'
            },
            {
                filter: 's.imgur.com'
            },
            {
                filter: 'instagram.com'
            },
            {
                filter: 'https://platform.twitter.com/widgets.js'
            },
        ],
        browser_spellcheck: true,
        contextmenu: true,
        autoresize_on_init: true,
        // max_height: 400,
        spellchecker_languages: 'English=en,Danish=da,Dutch=nl,Finnish=fi,French=fr_FR,' + 'German=de,Italian=it,Polish=pl,Portuguese=pt_BR,Spanish=es,Swedish=sv',
    });
    </script>
</body>

</html>