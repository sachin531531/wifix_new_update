<?php include 'db/dbConnection.php'; ?>

<?php
$id = $_GET['id'];

$sql = mysqli_query($connection, "SELECT * FROM user_tbl WHERE user_id = '$id'");
    $res = mysqli_fetch_array($sql);

$user_id = $res['user_id'];
$user_name = $res['user_name'];
$user_pwd = $res['user_pwd'];
$user_role = $res['user_type'];

$user1 = $res['user1'];
$customer = $res['customer'];
$suppliers = $res['suppliers'];
$jobCat = $res['jobCat'];
$job = $res['job'];
$labour = $res['labour'];
$labourPack = $res['labourPack'];
$grn = $res['grn'];
$release = $res['release1'];
$maReturn = $res['maReturn'];
$invSett = $res['invSett'];
$tax = $res['tax'];
$email = $res['email'];
$category = $res['category'];
$subCate = $res['subCate'];
$brand = $res['brand'];
$products = $res['products'];
$quote = $res['quote'];
$reQuote = $res['reQuote'];
$invoice = $res['invoice'];
$reInvoice = $res['reInvoice'];
$stockRepo = $res['stockRepo'];
$reProReport = $res['reProReport'];
$matReReport = $res['matReReport'];
$proReport = $res['proReport'];
$quoteReport = $res['quoteReport'];
$invReport = $res['invReport'];
$payReport = $res['payReport'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Edit User | WEFIX</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="assets/img/icon.ico" type="image/x-icon"/>
	
	<!-- Fonts and icons -->
	<script src="assets/js/plugin/webfont/webfont.min.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['assets/css/fonts.min.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
    </script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/atlantis.min.css">
	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link rel="stylesheet" href="assets/css/demo.css">
</head>
<body>
	<div class="wrapper">
		<!-- Navbar Header -->
		<?php include('header.php');?>	
		<!-- End Navbar -->
		<!-- Sidebar -->
		<?php include('sidebar.php');?>
		<!-- End Sidebar -->
		<style>
            section {
                display: flex;
                flex-direction: column;
                padding: 0.5em;
            }

            h1 {
              font-weight: 400;
              font-size: 2em;
              cursor: default;
              margin: 0 0 .5em 0;
              color:black;
            }

            .form-check {
              padding-left: 0;
            }

            input[type="checkbox"] {
              display: none;
              height: 0;
              width: 0;
            }
            input[type="checkbox"] + label {
              align-items: center;
              color: #e3e3e3;
              cursor: pointer;
              display: inline-flex;
              margin: 10px 0;
              position: relative;
            }
            input[type="checkbox"] + label > span {
              align-items: center;
              background: white;
              border: 2px solid blue;
              border-radius: 2px;
              display: inline-flex;
              height: 21px;
              justify-content: center;
              margin-right: 15px;
              transition: .25s ease;
              width: 21px;
            }
            input[type="checkbox"]:checked + label > span {
              animation: shrink-bounce ci-transition(fancy-hover);
            }
            input[type="checkbox"]:checked + label > span::before {
              animation: checkbox-check .125s .25s ease forwards;
              border-bottom: 3px solid transparent;
              border-radius: 2px;
              border-right: 3px solid transparent;
              content: "";
              left: 5px;
              position: absolute;
              top: 10px;
              transform: rotate(45deg);
              transform-origin: 0 100%;
            }

            @keyframes shrink-bounce {
              0% {
                transform: scale(1);
              }
              33% {
                transform: scale(0.85);
              }
              100% {
                transform: scale(1);
              }
            }
            @keyframes checkbox-check {
              0% {
                border-color: red;
                height: 0;
                transform: translate3d(0, 0, 0) rotate(45deg);
                width: 0;
              }
              33% {
                height: 0;
                transform: translate3d(0, 0, 0) rotate(45deg);
                width: 11px;
              }
              100% {
                border-color: red;
                height: 20px;
                transform: translate3d(0, -20px, 0) rotate(45deg);
                width: 11px;
              }
            }
        </style>
		<div class="main-panel">
			<div class="content">
				<div class="page-inner">
					<div class="page-header">
						<h4 class="page-title">EDIT USERS</h4>
					</div>
					<div class="row">
						<div class="col-md-12">
								<div class="card">
									<div class="card-body">
										<div class="row">
											<div class="col-md-12">
                                                <div class="">
													<input type="hidden" id="txt_userId" value="<?php echo $user_id; ?>">
												</div>
                                                <div class="">
													<input type="hidden" name="" id="pwd" value="<?php echo $user_pwd; ?>">
												</div>
												<div class="form-group">
													<label for="">User Name</label>
													<input type="text" class="form-control" id="txt_name" placeholder="Enter User Name" value="<?php echo $user_name;?>" name="txt_name">
												</div>
                                                <div class="form-group">
													<label for="">New Password</label>
													<input type="password" class="form-control" id="txt_pwd" value="<?php echo $user_pwd; ?>">
												</div>
												<div class="form-group">
													<label for="">Current Password</label>
													<input type="password" class="form-control" id="currpwd" onkeyup="myFunction()">
												</div>
												
												<div class="form-group">
													<label for="">User Role</label>
													<input type="text" class="form-control" id="txt_role" value="<?php echo $user_role;?>">
												</div>
											</div>
                                            <div class="col-md-3">
                                                <section>
                                                    <h1>User Setting</h1>
                                                    <input id='user' type='checkbox' />
                                                    <label for='user'>
                                                        <span></span>
                                                        List Users
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="col-md-3">
                                                <section>
                                                    <h1>Customers</h1>
                                                    <input id='customer' type='checkbox' />
                                                    <label for='customer'>
                                                        <span></span>
                                                        List Customers
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="col-md-3">
                                                <section>
                                                    <h1>Suppliers</h1>
                                                    <input id='suppliers' type='checkbox' />
                                                    <label for='suppliers'>
                                                        <span></span>
                                                        List Suppliers
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="col-md-3">
                                                <section>
                                                    <h1>Job List</h1>
                                                    <input id='jobCat' type='checkbox' />
                                                    <label for='jobCat'>
                                                        <span></span>
                                                        List Job Category
                                                    </label>
                                                    
                                                    <input id='job' type='checkbox' />
                                                    <label for='job'>
                                                        <span></span>
                                                        List Job
                                                    </label>
                                                </section>
                                            </div>
                                            
                                            <div class="col-md-3">
                                                <section>
                                                    <h1>Labour Packs</h1>
                                                    <input id='labour' type='checkbox' />
                                                    <label for='labour'>
                                                        <span></span>
                                                        List Labours
                                                    </label>
                                                    
                                                    <input id='labourPack' type='checkbox' />
                                                    <label for='labourPack'>
                                                        <span></span>
                                                        Labour Packs
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="col-md-3">
                                                <section>
                                                    <h1>Stock</h1>
                                                    <input id='grn' type='checkbox' />
                                                    <label for='grn'>
                                                        <span></span>
                                                        Good Receive Notes
                                                    </label>
                                                    
                                                    <input id='release' type='checkbox' />
                                                    <label for='release'>
                                                        <span></span>
                                                        Release Products
                                                    </label>
                                                    
                                                    <input id='maReturn' type='checkbox' />
                                                    <label for='maReturn'>
                                                        <span></span>
                                                        Material Return
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="col-md-3">
                                                <section>
                                                    <h1>Setting</h1>
                                                    <input id='invSett' type='checkbox' />
                                                    <label for='invSett'>
                                                        <span></span>
                                                        Invoice/Quotation Setting
                                                    </label>
                                                    
                                                    <input id='tax' type='checkbox' />
                                                    <label for='tax'>
                                                        <span></span>
                                                        Tax Setting
                                                    </label>
                                                    
                                                    <input id='email' type='checkbox' />
                                                    <label for='email'>
                                                        <span></span>
                                                        Quotation Email
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="col-md-3">
                                                <section>
                                                    <h1>Products</h1>
                                                    <input id='category' type='checkbox' />
                                                    <label for='category'>
                                                        <span></span>
                                                        List Categories
                                                    </label>
                                                    
                                                    <input id='subCate' type='checkbox' />
                                                    <label for='subCate'>
                                                        <span></span>
                                                        List SubCategories
                                                    </label>
                                                    
                                                    <input id='brand' type='checkbox' />
                                                    <label for='brand'>
                                                        <span></span>
                                                        List Brands
                                                    </label>
                                                    
                                                    <input id='products' type='checkbox' />
                                                    <label for='products'>
                                                        <span></span>
                                                        List Products
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="col-md-3">
                                                <section>
                                                    <h1>Quotation/Invoice</h1>
                                                    <input id='quote' type='checkbox' />
                                                    <label for='quote'>
                                                        <span></span>
                                                        List Quotation
                                                    </label>
                                                    
                                                    <input id='reQuote' type='checkbox' />
                                                    <label for='reQuote'>
                                                        <span></span>
                                                        List Revise Quotation
                                                    </label>
                                                    
                                                    <input id='invoice' type='checkbox' />
                                                    <label for='invoice'>
                                                        <span></span>
                                                        List Invoice
                                                    </label>
                                                    
                                                    <input id='reInvoice' type='checkbox' />
                                                    <label for='reInvoice'>
                                                        <span></span>
                                                        List Revise Invoice
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="col-md-3">
                                                <section>
                                                    <h1>Report</h1>
                                                    <input id='stockRepo' type='checkbox' />
                                                    <label for='stockRepo'>
                                                        <span></span>
                                                        Stock Reports
                                                    </label>
                                                    
                                                    <input id='reProReport' type='checkbox' />
                                                    <label for='reProReport'>
                                                        <span></span>
                                                        Reorder Pro. Reports
                                                    </label>
                                                    
                                                    <input id='matReReport' type='checkbox' />
                                                    <label for='matReReport'>
                                                        <span></span>
                                                        Material Return Reports
                                                    </label>
                                                    
                                                    <input id='proReport' type='checkbox' />
                                                    <label for='proReport'>
                                                        <span></span>
                                                        Product report
                                                    </label>
                                                    <input id='quoteReport' type='checkbox' />
                                                    <label for='quoteReport'>
                                                        <span></span>
                                                        Quotation Reports
                                                    </label>
                                                    <input id='invReport' type='checkbox' />
                                                    <label for='invReport'>
                                                        <span></span>
                                                        Invoice Reports
                                                    </label>
                                                    <input id='payReport' type='checkbox' />
                                                    <label for='payReport'>
                                                        <span></span>
                                                        Payment Reports
                                                    </label>
                                                </section>
                                            </div>
										</div>
									</div>
									<div class="card-action">
										<button id="editbtn" disabled class="btn btn-primary" onclick="edit()">
											<span class="btn-label">
												<i class="fa fa-check"></i>
											</span>
											Edit
										</button>
										<a href='list-users.php' class="btn btn-danger">
											<span class="btn-label">
												<i class="fa fa-times"></i>
											</span>
											Cancel
										</a>
									</div>
								</div>
						</div>
					</div>
				</div>
			</div>
			<!-- footer -->
			<?php include('footer.php');?>
			<!-- End footer -->
		</div>
		
		<!-- Custom template | don't include it in your project! -->
		<?php include('rightSidebar.php');?>
		<!-- End Custom template -->
	</div>
	<!--   Core JS Files   -->
	<script src="assets/js/core/jquery.3.2.1.min.js"></script>
	<script src="assets/js/core/popper.min.js"></script>
	<script src="assets/js/core/bootstrap.min.js"></script>
	<!-- jQuery UI -->
	<script src="assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script src="assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

	<!-- jQuery Scrollbar -->
	<script src="assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>
	<!-- Atlantis JS -->
	<script src="assets/js/atlantis.min.js"></script>
	<!-- Atlantis DEMO methods, don't include it in your project! -->
	<script src="assets/js/setting-demo2.js"></script>
	<!-- Atlantis DEMO methods, don't include it in your project! -->
	<script src="assets/js/setting-demo2.js"></script>
    <!-- Sweet Alert -->
	<script src="assets/js/plugin/sweetalert/sweetalert.min.js"></script>

	<script>
        function edit() {
            var name = document.getElementById("txt_name").value;
            var pwd = document.getElementById("txt_pwd").value;
            var role = document.getElementById("txt_role").value;
			var userId = document.getElementById("txt_userId").value;

            var user = 0;
            if (document.getElementById("user").checked) {
                user = 1;
            }
            var customer = 0;
            if (document.getElementById("customer").checked) {
                customer = 1;
            }
            var suppliers = 0;
            if (document.getElementById("suppliers").checked) {
                suppliers = 1;
            }
            var jobCat = 0;
            if (document.getElementById("jobCat").checked) {
                jobCat = 1;
            }
            var job = 0;
            if (document.getElementById("job").checked) {
                job = 1;
            }
            var labour = 0;
            if (document.getElementById("labour").checked) {
                labour = 1;
            }
            var labourPack = 0;
            if (document.getElementById("labourPack").checked) {
                labourPack = 1;
            }
            var grn = 0;
            if (document.getElementById("grn").checked) {
                grn = 1;
            }
            var release = 0;
            if (document.getElementById("release").checked) {
                release = 1;
            }
            var maReturn = 0;
            if (document.getElementById("maReturn").checked) {
                maReturn = 1;
            }
            var invSett = 0;
            if (document.getElementById("invSett").checked) {
                invSett = 1;
            }
            var tax = 0;
            if (document.getElementById("tax").checked) {
                tax = 1;
            }
            var email = 0;
            if (document.getElementById("email").checked) {
                email = 1;
            }
            var category = 0;
            if (document.getElementById("category").checked) {
                category = 1;
            }
            var subCate = 0;
            if (document.getElementById("subCate").checked) {
                subCate = 1;
            }
            var brand = 0;
            if (document.getElementById("brand").checked) {
                brand = 1;
            }
            var products = 0;
            if (document.getElementById("products").checked) {
                products = 1;
            }
            var quote = 0;
            if (document.getElementById("quote").checked) {
                quote = 1;
            }
            var reQuote = 0;
            if (document.getElementById("reQuote").checked) {
                reQuote = 1;
            }
            var invoice = 0;
            if (document.getElementById("invoice").checked) {
                invoice = 1;
            }
            var reInvoice = 0;
            if (document.getElementById("reInvoice").checked) {
                reInvoice = 1;
            }
            var stockRepo = 0;
            if (document.getElementById("stockRepo").checked) {
                stockRepo = 1;
            }
            var reProReport = 0;
            if (document.getElementById("reProReport").checked) {
                reProReport = 1;
            }
            var matReReport = 0;
            if (document.getElementById("matReReport").checked) {
                matReReport = 1;
            }
            var proReport = 0;
            if (document.getElementById("proReport").checked) {
                proReport = 1;
            }
            var quoteReport = 0;
            if (document.getElementById("quoteReport").checked) {
                quoteReport = 1;
            }
            var invReport = 0;
            if (document.getElementById("invReport").checked) {
                invReport = 1;
            }
            var payReport = 0;
            if (document.getElementById("payReport").checked) {
                payReport = 1;
            }

            obj = {
				"userId": userId,
                "name": name,
                "pwd": pwd,
                "role": role,
                "user":user,
                "customer":customer,
                "suppliers":suppliers,
                "jobCat":jobCat,
                "job":job,
                "labour":labour,
                "labourPack":labourPack,
                "grn":grn,
                "release":release,
                "maReturn":maReturn,
                "invSett":invSett,
                "tax":tax,
                "email":email,
                "category":category,
                "subCate":subCate,
                "brand":brand,
                "products":products,
                "quote":quote,
                "reQuote":reQuote,
                "invoice":invoice,
                "reInvoice":reInvoice,
                "stockRepo":stockRepo,
                "reProReport":reProReport,
                "matReReport":matReReport,
                "proReport":proReport,
                "quoteReport":quoteReport,
                "invReport":invReport,
                "payReport":payReport
            }
 
            $.ajax({
                url: "ajax/edit-userpro.php",
                type: "POST",
                data: {
                    data: obj
                },

                success: function(data) {
                    var res = JSON.parse(data);

                    if (res.status == 'success') {
                        var SweetAlert2Demo = function() {
                            var initDemos = function() {
                                swal({
                                    icon: "success",
                                    title: 'Success !',
                                    type: 'success',
                                    buttons: {
                                        confirm: {
                                            text: 'OK',
                                            className: 'btn btn-success'
                                        }
                                    }
                                }).then((Delete) => {
                                    if (Delete) {
                                        window.location.href = "list-users.php";
                                    } else {
                                        window.location.href = "list-users.php";
                                    }
                                });
                            };
                            return {
                                init: function() {
                                    initDemos();
                                },
                            };
                        }();
                        jQuery(document).ready(function() {
                            SweetAlert2Demo.init();
                        });
                    } else if (res.status == 'error') {
                        var SweetAlert2Demo = function() {
                            var initDemos = function() {
                                swal({
                                    icon: "error",
                                    title: 'Not Success !',
                                    type: 'error',
                                    buttons: {
                                        confirm: {
                                            text: 'OK',
                                            className: 'btn btn-danger'
                                        }
                                    }
                                });
                            };
                            return {
                                init: function() {
                                    initDemos();
                                },
                            };
                        }();
                        jQuery(document).ready(function() {
                            SweetAlert2Demo.init();
                        });
                    }
                },
                error: function(xhr, status, error) {
                    var errorMessage = xhr.status + ': ' + xhr.statusText;
                    var SweetAlert2Demo = function() {
                        var initDemos = function() {
                            swal({
                                icon: "error",
                                title: 'Not Success !' + errorMessage,
                                type: 'error',
                                buttons: {
                                    confirm: {
                                        text: 'OK',
                                        className: 'btn btn-danger'
                                    }
                                }
                            });
                        };
                        return {
                            init: function() {
                                initDemos();
                            },
                        };
                    }();
                    jQuery(document).ready(function() {
                        SweetAlert2Demo.init();
                    });
                }
            });
        }
    </script>

	<script>
	
	if (<?php echo $user1 ?> == 1) {
		document.getElementById("user").checked = true;
	}

	if (<?php echo $customer ?> == 1) {
		document.getElementById("customer").checked = true;
	}

	if (<?php echo $suppliers ?> == 1) {
		document.getElementById("suppliers").checked = true;
	}

	if (<?php echo $jobCat ?> == 1) {
		document.getElementById("jobCat").checked = true;
	}
	
	if (<?php echo $job ?> == 1) {
		document.getElementById("job").checked = true;
	}
	
	if (<?php echo $labour ?> == 1) {
		document.getElementById("labour").checked = true;
	}
	
	if (<?php echo $labourPack ?> == 1) {
		document.getElementById("labourPack").checked = true;
	}
	
	if (<?php echo $grn ?> == 1) {
		document.getElementById("grn").checked = true;
	}
	
	if (<?php echo $release ?> == 1) {
		document.getElementById("release").checked = true;
	}
	
	if (<?php echo $maReturn ?> == 1) {
		document.getElementById("maReturn").checked = true;
	}
	
	if (<?php echo $invSett ?> == 1) {
		document.getElementById("invSett").checked = true;
	}
	
	if (<?php echo $tax ?> == 1) {
		document.getElementById("tax").checked = true;
	}
	
	if (<?php echo $email ?> == 1) {
		document.getElementById("email").checked = true;
	}
	
	if (<?php echo $category ?> == 1) {
		document.getElementById("category").checked = true;
	}
	
	if (<?php echo $subCate ?> == 1) {
		document.getElementById("subCate").checked = true;
	}
	
	if (<?php echo $brand ?> == 1) {
		document.getElementById("brand").checked = true;
	}
	
	if (<?php echo $products ?> == 1) {
		document.getElementById("products").checked = true;
	}
	
	if (<?php echo $quote ?> == 1) {
		document.getElementById("quote").checked = true;
	}
	console.log(<?php echo $quote ?>);
	
	
	if (<?php echo $reQuote ?> == 1) {
		document.getElementById("reQuote").checked = true;
	}
	
	if (<?php echo $invoice ?> == 1) {
		document.getElementById("invoice").checked = true;
	}
	
	if (<?php echo $reInvoice ?> == 1) {
		document.getElementById("reInvoice").checked = true;
	}
	
	if (<?php echo $stockRepo ?> == 1) {
		document.getElementById("stockRepo").checked = true;
	}
	
	if (<?php echo $reProReport ?> == 1) {
		document.getElementById("reProReport").checked = true;
	}
	
	if (<?php echo $matReReport ?> == 1) {
		document.getElementById("matReReport").checked = true;
	}
	
	if (<?php echo $proReport ?> == 1) {
		document.getElementById("proReport").checked = true;
	}

	if (<?php echo $quoteReport ?> == 1) {
		document.getElementById("quoteReport").checked = true;
	}

	if (<?php echo $invReport ?> == 1) {
		document.getElementById("invReport").checked = true;
	}

	if (<?php echo $payReport ?> == 1) {
		document.getElementById("payReport").checked = true;
	}
	</script>

    <script>
        function myFunction() {
            var pwd=document.getElementById("pwd").value;
            var currpwd=document.getElementById("currpwd").value;
            if (pwd == currpwd) {
                document.getElementById("editbtn").disabled = false;
            }else{
                document.getElementById("editbtn").disabled = true;
            }
        }
    </script>
</body>
</html>