<!DOCTYPE html>
<html lang="en" id="printElement">

<?php 
session_start();

	if (!isset($_SESSION['user_name'])){
		header('Location: login.php?err=1');
	}
?>

<?php include 'db/dbConnection.php'; ?>

<?php
$id = $_GET['id'];
?>

<head>
    <meta charset="utf-8">
    <title>Invoice | WEFIX</title>
    <link rel="icon" href="assets/img/icon.ico" type="image/x-icon" />
    <script src="assets/js/plugin/webfont/webfont.min.js"></script>
    <script>
    WebFont.load({
        google: {
            "families": ["Lato:300,400,700,900"]
        },
        custom: {
            "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands",
                "simple-line-icons"
            ],
            urls: ['assets/css/fonts.min.css']
        },
        active: function() {
            sessionStorage.fonts = true;
        }
    });
    </script>
    <link rel="stylesheet" href="assets/css/invoiceStyle.css" media="all" />
    <link rel="stylesheet" href="assets/css/atlantis.min.css">
    <style>
    /* override styles when printing */
    @media print {
        #openWin {
            display: none;
        }

        #backbtn {
            display: none;
        }
    }
    </style>
</head>

<button type="button" id="openWin" class="btn btn-icon btn-round" title=""
    style="position:fixed;margin:auto; bottom:250px; right:10px; width:60px; height:60px; background-color:#5C55BF; border:none; cursor:pointer;"
    onclick="window.print();return false;">
    <i class="fas fa-print" style="font-size:180%; color:white;"></i>
</button>
<button type="button" class="btn btn-icon btn-round" id="backbtn"
    style="position:fixed;margin:auto; bottom:160px; right:10px; width:60px; height:60px; background-color:#5C55BF; border:none; cursor:pointer;"
    onclick="goBack()">
    <i class="fas fa-arrow-left" style="font-size:200%; color:white;"></i>
</button>

<body  style="background-color: white;">

    <?php
    $total = 0;

    $sql = mysqli_query($connection, "SELECT * FROM invoice_tbl,customer_tbl WHERE  invoice_tbl.invoice_id = '$id' AND customer_tbl.cus_id = invoice_tbl.invCus_id");
    $res = mysqli_fetch_array($sql);

    $added_user = $res['added_user'];
    $added_date = $res['added_date'];
    $added_time = $res['added_time'];
    $customer = $res['cus_name'];
    $custAddress = $res['address'];
    $custEmail = $res['cus_email'];
    $disc = $res['inv_disc'];
    $quote_no = $res['invoice_no'];
    $addTax = $res['add_tax'];
    $x = 1;
    $jobNumber=$res['invoice_job'];
    ?>

    <?php
    $sql1 = mysqli_query($connection, "SELECT * FROM invo_quotesetting_tbl WHERE id = 1");
    $res1 = mysqli_fetch_array($sql1);
    
    $name = $res1['name'];
    $number = $res1['number'];
    $email = $res1['email'];
    $image = $res1['image'];
    $address = $res1['address'];
    $footer = $res1['inFooter'];
    ?>

    <header class="clearfix"  style="background-color: white;">
        <div id="logo">
            <img src="image/<?php echo $image ?>">
        </div>
        <div id="company">
            <h2 class="name"><?php echo $name ?></h2>
            <div><?php echo $address ?></div>
            <div><?php echo $number ?></div>
            <div><a href="mailto:<?php echo $email ?>" target="_new"><?php echo $email ?></a></div>
        </div>
        </div>
    </header>
    <main style="background-color: white;">
        <div id="details" class="clearfix">
            <div id="client">
<!--                <div class="to">INVOICE TO:</div>-->
                <h2 class="name"><?php echo $customer ?></h2>
                <div class="address"><?php echo $custAddress ?></div>
                <div class="email"><a href="mailto:<?php echo $custEmail ?>" target="_new"><?php echo $custEmail ?></a></div>
            </div>
            <div id="invoice">
<!--                <h1>INVOICE</h1>-->
                <div class="date"><?php echo $added_date ?> : Date</div>
                <div class="date"><?php echo $quote_no ?> : Invoice Number</div>
                <div class="date">WEFIX : Project Name</div>
                <div class="date"> : PO Number</div>
                <div class="date"> : Contact Preson</div>
                <!-- <div class="date">Valid Date: 30/06/2014</div> -->
            </div>
        </div>
        <table border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th class="unit" style="border-bottom: 0.5px solid #AAAAAA;">S.NO</th>
                    <th class="desc" style="border-bottom: 0.5px solid #AAAAAA;">DESCRIPTION</th>
                    <th class="unit" style="border-bottom: 0.5px solid #AAAAAA;">UNIT</th>
                    <th class="qty" style="border-bottom: 0.5px solid #AAAAAA;">QTY</th>
                    <th class="unit" style="border-bottom: 0.5px solid #AAAAAA;">UNIT PRICE (Rs)</th>
                    <th class="total" style="border-bottom: 0.5px solid #AAAAAA;">AMOUNT (Rs)</th>
                </tr>
            </thead>
            <tbody>

                <?php
                    $sql="SELECT * From invoice_details_tbl,stock_tbl,products_tbl WHERE invoice_id = '$id' AND stock_tbl.stock_id = invoice_details_tbl.stock_id AND products_tbl.pro_id = stock_tbl.pro_id";
                    $result = mysqli_query($connection,$sql);
			        while($dataRow=mysqli_fetch_assoc($result)){ 
                        $total += $dataRow['totQty'] * $dataRow['invoice_price'];
                ?>

                <tr>
                    <td class="no"><?php echo $x ?></td>
                    <td class="desc">
                        <h3><?php echo $dataRow['pro_name']; ?></h3>
                        <?php echo $dataRow['invoice_desc']; ?>
                    </td>
                    <td class="unit"> <?php echo $dataRow['pro_unit']; ?> </td>
                    <td class="qty"><?php echo $dataRow['totQty']; ?></td>
                    <td class="unitPrice"><?php echo number_format($dataRow['invoice_price'],2); ?> </td>
                    <td class="total"><?php echo number_format($dataRow['totQty'] * $dataRow['invoice_price'],2); ?>
                    </td>
                </tr>
                <?php
                    $x++; 
                    } 
                ?>

                <?php
                    $sql="SELECT * FROM invoice_lapack_tbl,labourpack_tbl WHERE invoice_lapack_tbl.invoiceRef_id = '$id' AND labourpack_tbl.labourPack_id = invoice_lapack_tbl.invPack_id;";
                    $result = mysqli_query($connection,$sql);
			        while($dataRow=mysqli_fetch_assoc($result)){ 
                        $total += $dataRow['labourPack_price'];
                ?>

                <tr>
                    <td class="no"><?php echo $x ?></td>
                    <td class="desc">
                        <h3><?php echo $dataRow['labourPack_name']; ?></h3>
                    </td>
                    <td class="unit">-</td>
                    <td class="qty">-</td>
                    <td class="unitPrice"><?php echo number_format($dataRow['labourPack_price'],2); ?> </td>
                    <td class="total"><?php echo number_format($dataRow['labourPack_price'],2); ?> </td>
                </tr>
                <?php
                    $x++; 
                    } 
                ?>

                <?php
                    $sql="SELECT * From invoice_additional_tbl WHERE invoice_id = '$id';";
                    $result = mysqli_query($connection,$sql);
			        while($dataRow=mysqli_fetch_assoc($result)){ 
                        $total += $dataRow['additional_price'];
                ?>

                <tr>
                    <td class="no"><?php echo $x ?></td>
                    <td class="desc">
                        <h3><?php echo $dataRow['mainDec']; ?></h3>
                        <?php echo $dataRow['subDesc']; ?>
                    </td>
                    <td class="unit">-</td>
                    <td class="qty">-</td>
                    <td class="unitPrice"><?php echo number_format($dataRow['additional_price'],2); ?> </td>
                    <td class="total"><?php echo number_format($dataRow['additional_price'],2); ?> </td>
                </tr>
                <?php
                    $x++; 
                    } 
                ?>

            </tbody>
            <?php
            $totWithDis = 0;
            $totDisc = 0;
            $totTax = 0;
            $totWithTax = 0;

            $sql = mysqli_query($connection, "SELECT * FROM tax_tbl WHERE id = '1'");
            $res = mysqli_fetch_array($sql);
            $tax = $res['tax'];

            $totDisc += ($total * $disc / 100);
            $totWithDis += $total - $totDisc;

            if ($addTax == 0) {
                $totTax += ($totWithDis * $tax / 100);
                $totWithTax += $totWithDis + $totTax;
            }

            ?>
            <tfoot>
            <tr>
                <td colspan="1" style="text-align: left">Job Number</td>
                <td colspan="1"></td>
                <td colspan="3"></td>

                <td style="text-align:right;"><?php echo  $jobNumber?>
                </td>
            </tr>
            </tfoot>
            <?php if ($addTax == 0) { ?>
                <tfoot>
                <tr>

                    <td colspan="1" style="text-align: left">Total Amount (Rs.) </td>
                    <td colspan="1"></td>
                    <td colspan="3"></td>

                    <?php if ($addTax == 0) { ?>
                        <td style="text-align:right;"><?php echo number_format($totWithTax,2); ?>
                        </td>
                    <?php }else { ?>
                        <td style="text-align:right;"><?php echo number_format($totWithDis,2); ?>
                        </td>
                    <?php } ?>
                </tr>
                </tfoot>
            <?php } ?>
        </table>
        <div id="thanks">

<!--            --><?php //echo $footer ?>
            <p>We Thank you for your Business with us.........</p>
            <h5 style="margin-top:-10px">WEFIX <p style=" margin-top: -5px">by <br style="height: 1px">
                    APS Lanka (pvt.) Ltd.</p></h5>
            <p>APS Lanka Authorized by :</p>
        </div>
    </main>

    <!--   Core JS Files   -->
    <script src="assets/js/core/jquery.3.2.1.min.js"></script>
    <!-- <script src="assets/js/atlantis.min.js"></script> -->
    <script>
    function goBack() {
        window.location = 'list-invoice.php';
    }
    </script>
</body>

</html>
