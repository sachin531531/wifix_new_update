<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Add Job | Wefix</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <link rel="icon" href="assets/img/icon.ico" type="image/x-icon" />

    <!-- Fonts and icons -->
    <script src="assets/js/plugin/webfont/webfont.min.js"></script>
    <script>
    WebFont.load({
        google: {
            "families": ["Lato:300,400,700,900"]
        },
        custom: {
            "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands",
                "simple-line-icons"
            ],
            urls: ['assets/css/fonts.min.css']
        },
        active: function() {
            sessionStorage.fonts = true;
        }
    });
    </script>

    <style>
    .fileUpload {
        /* position: relative; */
        overflow: hidden;
        /* margin: 10px; */
    }

    .fileUpload input.upload {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }
    </style>

    <!-- CSS Files -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/atlantis.min.css">
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link rel="stylesheet" href="assets/css/demo.css">
</head>

<body>
    <div class="wrapper">
        <!-- Navbar Header -->
        <?php include('header.php');?>
        <!-- End Navbar -->
        <!-- Sidebar -->
        <?php include('sidebar.php');?>
        <!-- End Sidebar -->
        <div class="main-panel">
            <div class="content">
                <div class="page-inner">
                    <div class="page-header">
                        <h4 class="page-title">ADD JOB</h4>
                        <ul class="breadcrumbs">
                            <li class="nav-home">
                                <a href="index.php">
                                    <i class="flaticon-home"></i>
                                </a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item">
                                <a href="#">Job List</a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item">
                                <a href="#">Add Job</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-body">
                                    <ul class="nav nav-pills nav-secondary nav-pills-no-bd"
                                        id="pills-tab-without-border" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link" id="pills-home-tab-nobd" data-toggle="pill"
                                                href="#pills-home-nobd" role="tab" aria-controls="pills-home-nobd"
                                                aria-selected="true">Add Manually</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link active" id="pills-profile-tab-nobd" data-toggle="pill"
                                                href="#pills-profile-nobd" role="tab" aria-controls="pills-profile-nobd"
                                                aria-selected="false">Upload File</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content mt-2 mb-3" id="pills-without-border-tabContent">
                                        <div class="tab-pane fade" id="pills-home-nobd" role="tabpanel"
                                            aria-labelledby="pills-home-tab-nobd">
                                            <div class="col-md-12">
                                                <form action="add-jobManual.php" method="post"
                                                    enctype="multipart/form-data">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="">Date</label>
                                                                        <input type="date" class="form-control"
                                                                            name="txt_date" required>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="">Job No</label>
                                                                        <input type="text" class="form-control"
                                                                            name="txt_jNo"  pattern="[A-Za-z0-9]{1,}" required>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="">Customer Name</label>
                                                                        <input type="text" class="form-control"
                                                                            name="txt_cusName" pattern="[A-Za-z]{3,}" required>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="">Customer Category</label>
                                                                        <input type="text" class="form-control"
                                                                            name="txt_cusCategory"  pattern="[A-Za-z]{3,}" required>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for=""> Service Type </label>
                                                                        <select class="form-control" name="txt_seType">
                                                                            <option disabled selected hidden>Select
                                                                                Service Type</option>
                                                                            <?php
                                                                            $sql = mysqli_query($connection,"SELECT * FROM jobcategory_tbl");
                                                                            $row = mysqli_num_rows($sql);
                                                                            while ($row = mysqli_fetch_array($sql)){   echo "<option value='". $row['jobCat_name'] ."'>" .$row['jobCat_name'] ."</option>" ;
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="">Job Type</label>
                                                                        <input type="text" class="form-control"
                                                                            name="txt_jType"  pattern="[A-Za-z]{3,}" required>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="">Job Status</label>
                                                                        <input type="text" class="form-control"
                                                                            name="txt_jStatus"  pattern="[A-Za-z]{3,}" required>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="">Job Created On</label>
                                                                        <input type="text" class="form-control"
                                                                            name="txt_jCreateOn"  pattern="[A-Za-z0-9]{3,}" required>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="">Assigned On</label>
                                                                        <input type="text" class="form-control"
                                                                            name="txt_AssignOn" pattern="[A-Za-z0-9]{3,}">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="">Dispatched On</label>
                                                                        <input type="text" class="form-control"
                                                                            name="txt_disOn" pattern="[A-Za-z0-9]{3,}">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="">Completed On</label>
                                                                        <input type="text" class="form-control"
                                                                            name="txt_comOn" pattern="[A-Za-z0-9]{3,}" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="card-action">
                                                            <button class="btn btn-primary" type="submit">
                                                                <span class="btn-label">
                                                                    <i class="fa fa-check"></i>
                                                                </span>
                                                                Add Job
                                                            </button>
                                                            <a href='list-job.php' class="btn btn-danger">
                                                                <span class="btn-label">
                                                                    <i class="fa fa-times"></i>
                                                                </span>
                                                                Cancel
                                                            </a>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                        <div class="tab-pane fade  show active" id="pills-profile-nobd" role="tabpanel"
                                            aria-labelledby="pills-profile-tab-nobd">
                                            <div class="col-md-12">
                                                <form action="add-jobPro.php" method="post"
                                                    enctype="multipart/form-data">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label for="">File (CSV Format)</label>
                                                                        <div class="input-group">
                                                                            <div class="input-group-prepend">
                                                                                <div class="fileUpload btn btn-primary">
                                                                                    <span>Upload</span>
                                                                                    <input id="uploadBtn" type="file"
                                                                                        class="upload" name="file" />
                                                                                </div>
                                                                            </div>
                                                                            <input type="text" id="uploadFile"
                                                                                class="form-control"
                                                                                placeholder="Choose File" aria-label=""
                                                                                aria-describedby="basic-addon1"
                                                                                disabled="disabled">
                                                                        </div>
                                                                        <script>
                                                                        document.getElementById("uploadBtn").onchange = function() {
                                                                                document.getElementById("uploadFile").value = this.value;
                                                                            };
                                                                        </script>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="card-action">
                                                            <button class="btn btn-primary" type="submit">
                                                                <span class="btn-label">
                                                                    <i class="fa fa-check"></i>
                                                                </span>
                                                                Add Job
                                                            </button>
                                                            <a href='list-job.php' class="btn btn-danger">
                                                                <span class="btn-label">
                                                                    <i class="fa fa-times"></i>
                                                                </span>
                                                                Cancel
                                                            </a>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer -->
            <?php include('footer.php');?>
            <!-- End footer -->
        </div>

        <!-- Custom template | don't include it in your project! -->
        <?php include('rightSidebar.php');?>
        <!-- End Custom template -->
    </div>
    <!--   Core JS Files   -->
    <script src="assets/js/core/jquery.3.2.1.min.js"></script>
    <script src="assets/js/core/popper.min.js"></script>
    <script src="assets/js/core/bootstrap.min.js"></script>
    <!-- jQuery UI -->
    <script src="assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <script src="assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

    <!-- jQuery Scrollbar -->
    <script src="assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <!-- Atlantis JS -->
    <script src="assets/js/atlantis.min.js"></script>
    <!-- Atlantis DEMO methods, don't include it in your project! -->
    <script src="assets/js/setting-demo2.js"></script>
</body>

</html>