<?php 
include 'db/dbConnection.php';  
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Wefix</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <link rel="icon" href="assets/img/icon.ico" type="image/x-icon" />

    <!-- Fonts and icons -->
    <script src="assets/js/plugin/webfont/webfont.min.js"></script>
    <script>
    WebFont.load({
        google: {
            "families": ["Lato:300,400,700,900"]
        },
        custom: {
            "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands",
                "simple-line-icons"
            ],
            urls: ['assets/css/fonts.min.css']
        },
        active: function() {
            sessionStorage.fonts = true;
        }
    });
    </script>

    <!-- CSS Files -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/atlantis.min.css">
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link rel="stylesheet" href="assets/css/demo.css">
</head>

<body>
    <div class="wrapper">
        <!-- Navbar Header -->
        <?php include('header.php');?>
        <!-- End Navbar -->
        <!-- Sidebar -->
        <?php include('sidebar.php');?>
        <!-- End Sidebar -->
        <div class="main-panel">
            <div class="content">
                <div class="page-inner">
                    <div class="page-header">
                        <h4 class="page-title">List Invoice</h4>
                        <ul class="breadcrumbs">
                            <li class="nav-home">
                                <a href="index.php">
                                    <i class="flaticon-home"></i>
                                </a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item">
                                <a href="#">List Invoice</a>
                            </li>
                            <li class="separator">
                                <i class="flaticon-right-arrow"></i>
                            </li>
                            <li class="nav-item">
                                <a href="#">List Invoice</a>
                            </li>
                        </ul>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="d-flex align-items-center">
                                        <a class=" ml-auto" href="invoice.php">
                                            <button class="btn btn-primary btn-round">
                                                <i class="fa fa-plus"></i>
                                                New Invoice
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table id="example" class="display table table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th>NO</th>
                                                    <th>Job No</th>
                                                    <th>Customer Name</th>
                                                    <th>Service Type</th>
                                                    <th>Job Status</th>
                                                    <th>Advance Payment</th>
                                                    <th>Total</th>
                                                    <th>Pending Payment</th>
                                                    <th>Date</th>
                                                    <th>Pending Date</th>
                                                    <th>Payment Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <!-- <th>NO</th>
                                                    <th>Job No</th>
                                                    <th>Customer Name</th>
                                                    <th>Service Type</th>
                                                    <th>Job Status</th>
                                                    <th>Date</th>
                                                    <th>Action</th> -->
                                                </tr>
                                            </tfoot>
                                            <tbody>

                                            <?php
                                            function dateDiffInDays($date1, $date2) {
                                                $diff = strtotime($date2) - strtotime($date1);
                                                return abs(round($diff / 86400));
                                            }
                                            ?>

                                            <?php
                                            $sql = mysqli_query($connection, "SELECT * FROM tax_tbl WHERE id = '1'");
                                            $res = mysqli_fetch_array($sql);
                                            $tax = $res['tax'];

											$sql="SELECT * FROM invoice_tbl,job_tbl,quatation_tbl WHERE job_tbl.job_id=invoice_tbl.invoice_job AND quatation_tbl.quate_id = invoice_tbl.quoteRef_id";
											$result = mysqli_query($connection,$sql);

											while($dataRow=mysqli_fetch_assoc($result)){
                                                $revise = $dataRow['revise_invoice'];
                                                $reAddQt = $dataRow['reInvoice'];
                                                $pay = $dataRow['payment'];

                                                $invID = $dataRow['invoice_id'];
                                                $costPrice = 0.0;
                                                $total = 0.0;
                                                $addTax = $dataRow['add_tax'];
                                                $disc = $dataRow['inv_disc'];
                                                
                                                $paymentStatus="Pending";
                                                if( $pay ==1){
                                                    $paymentStatus="Success";
                                                }
                                                $date2 = date("m/d/Y");
                                                $date1 = $dataRow['added_date'];
                                                $dateDiff = dateDiffInDays($date1, $date2);

                                                // ------- invoice lapack------
                                                $sql1="SELECT * FROM invoice_lapack_tbl,labourpack_tbl WHERE invoice_lapack_tbl.invoiceRef_id = '$invID' AND labourpack_tbl.labourPack_id = invoice_lapack_tbl.invPack_id;";
                                                $result1 = mysqli_query($connection,$sql1);
			                                    while($dataRow1=mysqli_fetch_assoc($result1)){ 
                                                    $total += $dataRow1['labourPack_price'];
                                                    $costPrice += $dataRow1['labourPack_cost'];
                                                }

                                                // ------- invoice Details ------
                                                $sql2="SELECT * From invoice_details_tbl,products_tbl,stock_tbl WHERE invoice_id = '$invID' AND products_tbl.pro_id = invoice_details_tbl.pro_code AND stock_tbl.stock_id = invoice_details_tbl.stock_id";
                                                $result2 = mysqli_query($connection,$sql2);
			                                    while($dataRow2=mysqli_fetch_assoc($result2)){ 
                                                    $total += $dataRow2['totQty'] * $dataRow2['invoice_price'];
                                                    $costPrice += $dataRow2['totQty'] * $dataRow2['stock_proPrice'];
                                                }

                                                // ------- invoice Additional ------
                                                $sql3="SELECT * FROM invoice_additional_tbl WHERE invoice_id = '$invID';";
                                                $result3 = mysqli_query($connection,$sql3);
			                                    while($dataRow3=mysqli_fetch_assoc($result3)){ 
                                                    $total += $dataRow3['additional_price'];
                                                }

                                                // ------- Tax & discount ------
                                                $total -= ($total * $disc / 100);

                                                if ($addTax == 0) {
                                                    $total += ($total * $tax / 100);
                                                }

                                                // ------- Payment Details ------
                                                $totalPay = 0.0;
                                                $sql4="SELECT * FROM payment_tbl WHERE pay_inv = '$invID';";
                                                $result4 = mysqli_query($connection,$sql4);
			                                    while($dataRow4=mysqli_fetch_assoc($result4)){ 
                                                    $totalPay += $dataRow4['pay_price'];
                                                }
                                                // $totalPay = $totalPay - $dataRow['quote_advance'];
                           
                                            echo "<tr>";    
												echo "<td >".$dataRow['invoice_no']."</td>";
												echo "<td >".$dataRow['job_no']."</td>";
												echo "<td >".$dataRow['customer_name']."</td>";    
												echo "<td >".$dataRow['service_type']."</td>";
                                                echo "<td >".$dataRow['job_status']."</td>";
                                                echo "<td >".number_format($dataRow['quote_advance'],2)."</td>";
                                                echo "<td >".number_format($total,2)."</td>";
                                                echo "<td >".number_format($total - $totalPay - $dataRow['quote_advance'],2)."</td>";
                                                echo "<td >".$dataRow['added_date']."</td>";
                                                echo "<td >".$dateDiff."</td>";
                                                echo "<td >".$paymentStatus."</td>";
                                                echo "<td>
														<div class=\"form-button-action\">

                                                            <a style='width:20px; height:20px; text-align:center; margin-left: 0px;' href='invoice-details.php?id=$dataRow[invoice_id]'>

                                                                <i style=\"font-size: 25px;\" data-toggle=\"tooltip\" title=\"View Details\" data-original-title=\"View Details\" class=\"fa fa-eye\"></i>

                                                            </a>";

                                                if ($reAddQt == 0) {

                                                    echo "<a style='width:20px; height:20px; text-align:center; margin-left: 25px;' href='invoice-reAdd.php?id=$dataRow[invoice_id]'>

														<i style=\"font-size: 22px;\" data-toggle=\"tooltip\" title=\"Re Add\" data-original-title=\"Re Add\" class=\"fas fa-redo-alt\"></i>

                                                    </a>";
                                                }

                                                if ($pay == 0) {
                                                            
                                                    echo "<a style='width:20px; height:20px; text-align:center; margin-left: 25px;' href='#' onclick='addPayment($dataRow[invoice_id],$total - $dataRow[quote_advance],$total - $totalPay - $dataRow[quote_advance])'>

                                                            <i style=\"font-size: 25px;\" data-toggle=\"tooltip\" title=\"payment\" data-original-title=\"payment\" class=\"fas fa-file-invoice-dollar\"></i>

                                                        </a>";

                                                    if ($revise == 0) {
                                                            
                                                        echo "<a style='width:20px; height:20px; text-align:center; margin-left: 25px;' href='invoice-revise.php?id=$dataRow[invoice_id]'>

                                                            <i style=\"font-size: 25px;\" data-toggle=\"tooltip\" title=\"Revise\" data-original-title=\"Revise\" class=\"fas fa-arrow-circle-left\"></i>

                                                        </a>";
                                                    }
                                                }
                                                echo "</div> </td>";
											echo "</tr>";
											}
											?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer -->
            <?php include('footer.php');?>
            <!-- End footer -->

            <!-- Add discount Modal -->
            <div class="modal fade" id="addDiscount" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <form action="invoice-pay.php" method="post" enctype="multipart/form-data">
                        <div class="modal-content">
                            <div class="modal-header no-bd">
                                <h4 class="modal-title">
                                    <span class="fw-mediumbold"> INVOICE </span>
                                    <span class="fw-light"> PAYMENT </span>
                                </h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button> 
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group" style="display:none;">
                                            <input type="text" class="form-control" id="txtID" name="txtID">
                                            <input type="text" class="form-control" id="txtTotal" name="txtTotal">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Payment Price</label>
                                            <input type="text" value="0" class="form-control" id="txtPayment" name="txtPayment">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer no-bd">
                                <button class="btn btn-primary" type="submit"> Pay </button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal"> Close </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- end Modal -->
        </div>

        <!-- Custom template | don't include it in your project! -->
        <?php include('rightSidebar.php');?>
        <!-- End Custom template -->
    </div>
    <!--   Core JS Files   -->
    <script src="assets/js/core/jquery.3.2.1.min.js"></script>
    <script src="assets/js/core/popper.min.js"></script>
    <script src="assets/js/core/bootstrap.min.js"></script>
    <!-- jQuery UI -->
    <script src="assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <script src="assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

    <!-- jQuery Scrollbar -->
    <script src="assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <!-- Datatables -->
    <script src="assets/js/plugin/datatables/datatables.min.js"></script>
    <!-- Atlantis JS -->
    <script src="assets/js/atlantis.min.js"></script>
    <!-- Atlantis DEMO methods, don't include it in your project! -->
    <script src="assets/js/setting-demo2.js"></script>

    <!-- Sweet Alert -->
    <script src="assets/js/plugin/sweetalert/sweetalert.min.js"></script>
    
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>

    <script>
    $(document).ready(function() {
        $('#example').DataTable({
            
        });
    });

    function addPayment(id,total,advance) { 
        document.getElementById("txtID").value = id;
        document.getElementById("txtTotal").value = total;
        document.getElementById("txtPayment").value = advance;
        $('#addDiscount').modal('show');
    }
    </script>
</body>

</html>
