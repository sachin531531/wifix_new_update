<?php 
include 'db/dbConnection.php';  
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Wefix</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <link rel="icon" href="assets/img/icon.ico" type="image/x-icon" />

    <!-- Fonts and icons -->
    <script src="assets/js/plugin/webfont/webfont.min.js"></script>
    <script>
    WebFont.load({
        google: {
            "families": ["Lato:300,400,700,900"]
        },
        custom: {
            "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands","simple-line-icons"
            ],
            urls: ['assets/css/fonts.min.css']
        },
        active: function() {
            sessionStorage.fonts = true;
        }
    });
    </script>

    <!-- CSS Files -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/atlantis.min.css">

    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link rel="stylesheet" href="assets/css/demo.css">
</head>

<body>
    <div class="wrapper">
        <!-- Navbar -->
        <?php include('header.php');?>
        <!-- End Navbar -->

        <!-- Sidebar -->
        <?php include('sidebar.php');?>
        <!-- End Sidebar -->

        <div class="main-panel">
            <div class="content">
                <div class="panel-header bg-primary-gradient">
                    <div class="page-inner py-5">
                        <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                            <div>
                                <h2 class="text-white pb-2 fw-bold">Dashboard</h2>
                                <!-- <h5 class="text-white op-7 mb-2">Free Bootstrap 4 Admin Dashboard</h5> -->
                            </div>
                            <div class="ml-md-auto py-2 py-md-0">
                                <!-- <a href="#" class="btn btn-white btn-border btn-round mr-2">Manage</a> -->
                                <!-- <a href="#" class="btn btn-secondary btn-round">Add Customer</a> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="page-inner mt--5">
                    <div class="row mt--2">
                        <div class="col-md-12">
                            <div class="card full-height">
                                <div class="card-body">
                                    <div class="card-title">Overall statistics</div>
                                    <div class="card-category">Daily information about statistics in system</div>
                                    <div class="d-flex flex-wrap justify-content-around pb-2 pt-4">
                                        <div class="px-2 pb-2 pb-md-0 text-center">
                                            <div id="circles-1"></div>
                                            <h6 class="fw-bold mt-3 mb-0">Reorder Products</h6>
                                        </div>
                                        <div class="px-2 pb-2 pb-md-0 text-center">
                                            <div id="circles-4"></div>
                                            <h6 class="fw-bold mt-3 mb-0">Pending Jobs</h6>
                                        </div>
                                        <div class="px-2 pb-2 pb-md-0 text-center">
                                            <div id="circles-2"></div>
                                            <h6 class="fw-bold mt-3 mb-0">Payed Invoice</h6>
                                        </div>
                                        <!-- <div class="px-2 pb-2 pb-md-0 text-center">
                                            <div id="circles-5"></div>
                                            <h6 class="fw-bold mt-3 mb-0">Quotation Accepted Jobs</h6>
                                        </div> -->
                                        <div class="px-2 pb-2 pb-md-0 text-center">
                                            <div id="circles-6"></div>
                                            <h6 class="fw-bold mt-3 mb-0">Invoice Send Jobs</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php 
            $sendINV = 0;
            $sendQTE = 0;
            $pending = 0;
            $accepted = 0;
            $sendINVrand = 0;
            $sendQTErand = 0;
            $pendingrand = 0;
            $acceptedrand = 0;

            $sql="SELECT * From job_tbl";
            $result = mysqli_query($connection,$sql);
            while($dataRow=mysqli_fetch_assoc($result)){
                if ($dataRow['job_status'] == "Send Invoice") {
                    ++$sendINV ;
                    $sendINVrand = rand(1,80);
                }elseif ($dataRow['job_status'] == "Quotation Accepted") {
                    ++$accepted ;
                    $acceptedrand = rand(5,90);
                }elseif ($dataRow['job_status'] == "pending") {
                    ++$pending ;
                    $pendingrand = rand(10,59);
                }
            }

            $sql8="SELECT * From invoice_tbl";
            $result8 = mysqli_query($connection,$sql8);
            while($dataRow8=mysqli_fetch_assoc($result8)){
                if ($dataRow8['payment'] == 1) {
                    ++$sendQTE ;
                    $sendQTErand = rand(15,69);
                }
            }
            ?>

            <!-- footer -->
            <?php include('footer.php');?>
            <!-- End footer -->
        </div>

        <!-- Custom template | don't include it in your project! -->
        <?php include('rightSidebar.php');?>
        <!-- End Custom template -->
    </div>
    <!--   Core JS Files   -->
    <script src="assets/js/core/jquery.3.2.1.min.js"></script>
    <script src="assets/js/core/popper.min.js"></script>
    <script src="assets/js/core/bootstrap.min.js"></script>

    <!-- jQuery UI -->
    <script src="assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <script src="assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

    <!-- jQuery Scrollbar -->
    <script src="assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>


    <!-- Chart JS -->
    <script src="assets/js/plugin/chart.js/chart.min.js"></script>

    <!-- jQuery Sparkline -->
    <script src="assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js"></script>

    <!-- Chart Circle -->
    <script src="assets/js/plugin/chart-circle/circles.min.js"></script>

    <!-- Datatables -->
    <script src="assets/js/plugin/datatables/datatables.min.js"></script>

    <!-- Bootstrap Notify -->
    <script src="assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js"></script>

    <!-- jQuery Vector Maps -->
    <!-- <script src="assets/js/plugin/jqvmap/jquery.vmap.min.js"></script>
	<script src="assets/js/plugin/jqvmap/maps/jquery.vmap.world.js"></script> -->

    <!-- Sweet Alert -->
    <script src="assets/js/plugin/sweetalert/sweetalert.min.js"></script>

    <!-- Atlantis JS -->
    <script src="assets/js/atlantis.min.js"></script>

    <!-- Atlantis DEMO methods, don't include it in your project! -->
    <script src="assets/js/setting-demo.js"></script>
    <script src="assets/js/demo.js"></script>

    <script>
    Circles.create({
        id: 'circles-1',
        radius: 45,
        value: <?php echo $x; ?>,
        maxValue: 100,
        width: 7,
        text: <?php echo $x; ?>,
        colors: ['#f1f1f1', '#FF9E27'],
        duration: 400,
        wrpClass: 'circles-wrp',
        textClass: 'circles-text',
        styleWrapper: true,
        styleText: true
    })

    Circles.create({
        id: 'circles-2',
        radius: 45,
        value: <?php echo $sendQTErand; ?>,
        maxValue: 100,
        width: 7,
        text: <?php echo $sendQTE; ?>,
        colors: ['#f1f1f1', '#2BB930'],
        duration: 400,
        wrpClass: 'circles-wrp',
        textClass: 'circles-text',
        styleWrapper: true,
        styleText: true
    })

    Circles.create({
        id: 'circles-4',
        radius: 45,
        value: <?php echo $pendingrand; ?>,
        maxValue: 100,
        width: 7,
        text: <?php echo $pending; ?>,
        colors: ['#f1f1f1', '#FF9E27'],
        duration: 400,
        wrpClass: 'circles-wrp',
        textClass: 'circles-text',
        styleWrapper: true,
        styleText: true
    })

    // Circles.create({
    //     id: 'circles-5',
    //     radius: 45,
    //     value: <?php echo $acceptedrand; ?>,
    //     maxValue: 100,
    //     width: 7,
    //     text: <?php echo $accepted; ?>,
    //     colors: ['#f1f1f1', '#2BB930'],
    //     duration: 400,
    //     wrpClass: 'circles-wrp',
    //     textClass: 'circles-text',
    //     styleWrapper: true,
    //     styleText: true
    // })

    Circles.create({
        id: 'circles-6',
        radius: 45,
        value: <?php echo $sendINVrand; ?>,
        maxValue: 100,
        width: 7,
        text: <?php echo $sendINV; ?>,
        colors: ['#f1f1f1', '#F25961'],
        duration: 400,
        wrpClass: 'circles-wrp',
        textClass: 'circles-text',
        styleWrapper: true,
        styleText: true
    })
    </script>
</body>

</html>