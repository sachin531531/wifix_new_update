<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Purchase Order | WEFIX</title>
    <link rel="icon" href="assets/img/icon.ico" type="image/x-icon" />
    <script src="assets/js/plugin/webfont/webfont.min.js"></script>
    <script>
    WebFont.load({
        google: {
            "families": ["Lato:300,400,700,900"]
        },
        custom: {
            "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands",
                "simple-line-icons"
            ],
            urls: ['assets/css/fonts.min.css']
        },
        active: function() {
            sessionStorage.fonts = true;
        }
    });
    </script>
    <link rel="stylesheet" href="assets/css/invoiceStyle.css" media="all" />
    <link rel="stylesheet" href="assets/css/atlantis.min.css">
</head>

<button type="button" id="openWin" class="btn btn-icon btn-round" title=""
    style="position:fixed;margin:auto; bottom:250px; right:10px; width:60px; height:60px; background-color:#5C55BF; border:none; cursor:pointer;"
    onclick="">
    <i class="fas fa-print" style="font-size:180%; color:white;"></i>
</button>
<button type="button" data-toggle-fullscreen class="btn btn-icon btn-round" title=""
    style="position:fixed;margin:auto; bottom:160px; right:10px; width:60px; height:60px; background-color:#5C55BF; border:none; cursor:pointer;"
    onclick="goBack()">
    <i class="fas fa-arrow-left" style="font-size:180%; color:white;" data-toggle-fullscreen></i>
</button>

<body>
    <header class="clearfix">
        <div id="logo">
            <img src="logo.png">
        </div>
        <div id="company">
            <h2 class="name">Company Name</h2>
            <div>455 Foggy Heights, AZ 85004, US</div>
            <div>(602) 519-0450</div>
            <div><a href="mailto:company@example.com">company@example.com</a></div>
        </div>
        </div>
    </header>
    <main>
        <div id="details" class="clearfix">
            <div id="client">
                <div class="to">SHIP TO:</div>
                <h2 class="name">John Doe</h2>
                <div class="address">796 Silver Harbour, TX 79273, US</div>
                <div class="email"><a href="mailto:john@example.com">john@example.com</a></div>
            </div>
            <div id="invoice">
                <h1 style="font-size: 2.1em; font-weight:bold;">PURCHASE ORDER</h1>
                <div class="date">Date: 01/06/2014</div>
                <div class="date">P.O Number: 45455</div>
                <div class="date">Customer ID: 30/06/2014</div>
            </div>
        </div>
        <table border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th class="no">#</th>
                    <th class="desc">DESCRIPTION</th>
                    <th class="unit">UNIT PRICE</th>
                    <th class="qty">QUANTITY</th>
                    <th class="total">TOTAL</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="no">01</td>
                    <td class="desc">
                        <h3>Website Design</h3>Creating a recognizable design solution based on the company's existing
                        visual identity
                    </td>
                    <td class="unit">$40.00</td>
                    <td class="qty">30</td>
                    <td class="total">$1,200.00</td>
                </tr>
                <tr>
                    <td class="no">02</td>
                    <td class="desc">
                        <h3>Website Development</h3>Developing a Content Management System-based Website
                    </td>
                    <td class="unit">$40.00</td>
                    <td class="qty">80</td>
                    <td class="total">$3,200.00</td>
                </tr>
                <tr>
                    <td class="no">03</td>
                    <td class="desc">
                        <h3>Search Engines Optimization</h3>Optimize the site for search engines (SEO)
                    </td>
                    <td class="unit">$40.00</td>
                    <td class="qty">20</td>
                    <td class="total">$800.00</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="2"></td>
                    <td colspan="2">SUBTOTAL</td>
                    <td>$5,200.00</td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td colspan="2">TAX 25%</td>
                    <td>$1,300.00</td>
                </tr>
                <tr>
                    <td colspan="2"></td>
                    <td colspan="2">GRAND TOTAL</td>
                    <td>$6,500.00</td>
                </tr>
            </tfoot>
        </table>
        <div id="thanks">Thank you!</div>
        <div id="notices">
            <div>NOTICE:</div>
            <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
        </div>
    </main>
    <footer>
        Invoice was created on a computer and is valid without the signature and seal.
    </footer>

    <!--   Core JS Files   -->
    <script src="assets/js/core/jquery.3.2.1.min.js"></script>
    <script src="assets/js/atlantis.min.js"></script>
    <script>
    function goBack() {
        window.history.back();
    }
    </script>
</body>

</html>