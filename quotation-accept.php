<?php 
    include 'db/dbConnection.php';
    $id =  $_POST['txtAcceptID'];
    $advance =  $_POST['txtDiscount'];

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>WEFIX</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <link rel="icon" href="assets/img/icon.ico" type="image/x-icon" />

    <!-- Fonts and icons -->
    <script src="assets/js/plugin/webfont/webfont.min.js"></script>
    <script>
    WebFont.load({
        google: {
            "families": ["Lato:300,400,700,900"]
        },
        custom: {
            "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands",
                "simple-line-icons"
            ],
            urls: ['assets/css/fonts.min.css']
        },
        active: function() {
            sessionStorage.fonts = true;
        }
    });
    </script>

    <!--   Core JS Files   -->
    <script src="assets/js/core/jquery.3.2.1.min.js"></script>
    <!-- CSS Files -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/atlantis.min.css">
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link rel="stylesheet" href="assets/css/demo.css">
</head>

<body>
    <div class="wrapper">
        <!-- Navbar Header -->
        <?php include('header.php');?>
        <!-- End Navbar -->
        <!-- Sidebar -->
        <?php include('sidebar.php');?>
        <!-- End Sidebar -->
        <div class="main-panel">
            <div class="content">
                <?php 
                
                    $sql = "UPDATE quatation_tbl SET 
                                   accept_qte='1',
                                   quote_advance='$advance'
                                   WHERE 
                                   quate_id='$id'";
                
                                   
                    ?>

                <?php  if ($connection->query($sql)===true){

                    $sql = mysqli_query($connection, "SELECT * FROM quatation_tbl WHERE quate_id = '$id'");
                    $res = mysqli_fetch_array($sql);
                    $QutNum = $res['quote_no'];
                    
                    $from = "kasunviduranga.kv@gmail.com";
                    $number = $QutNum;

                    $headers = "From: $from";
                    $headers = "From: " . $from . "\r\n";
                    $headers .= "Reply-To: ". $from . "\r\n";
                    $headers .= "MIME-Version: 1.0\r\n";
                    $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

                    $subject = "You have a Quotation Accepted message from Wefix Software.";

                    $logo = 'http://test.skyydigital.com/piumiTours/image/logo.jpg';
                    $link = 'http://test.skyydigital.com/piumiTours/';

                    $body = "<html lang='en'><body>";
                    $body .= "<table style='width: 100%;'>";
                    $body .= "<thead style='text-align: center;'><tr><td style='border:none;' colspan='2'>";
                    $body .= "<a href='{$link}'><img src='{$logo}' alt=''></a><br><br>";
                    $body .= "</td></tr></thead><tbody><tr>";
                    $body .= "<td style='border:none;'><strong>Email:</strong> {$from}</td>";
                    $body .= "</tr>";
                    $body .= "<tr><td style='border:none;'><strong>Quotation Number:</strong> {$number}</td></tr>";
                    $body .= "<tr><td></td></tr>";
                    $body .= "</tbody></table>";
                    $body .= "</body></html>";

                    $sql="SELECT * From email_table";
					$result = mysqli_query($connection,$sql);
                    while($dataRow=mysqli_fetch_assoc($result)){
                        $to =$dataRow['mail'];
                        $send = mail($to, $subject, $body, $headers);
                    }
                    
                ?> 
                      
                <script>
                var SweetAlert2Demo = function() {

                    var initDemos = function() {
                        swal({
                            icon: "success",
                            title: 'Success !',
                            type: 'success',
                            buttons: {
                                confirm: {
                                    text: 'OK',
                                    className: 'btn btn-success'
                                }
                            }
                           
                        }).then((Delete) => {
                            var advance = <?php echo $advance ?>;
                           
                            if (Delete) {
                                if(advance>0){
                                    window.location.replace("print-bill.php?id=<?php echo $id ?>");
                                }else if(advance==0){
                                    window.location.replace("list-quotation.php");
                                }
                            } else {
                                if($advance>0){
                                    window.location.replace("print-bill.php?id=<?php echo $id ?>");
                                }else if($advance==0){
                                    window.location.replace("list-quotation.php");
                                }
                            }
                        });
                    };
                    return {
                        init: function() {
                            initDemos();
                        },
                    };
                }();
                
                jQuery(document).ready(function() {
                    SweetAlert2Demo.init();
                });
                </script>

                <?php }else{ ?>
                <script>
                var SweetAlert2Demo = function() {

                    var initDemos = function() {
                        swal({
                            icon: "error",
                            title: 'Not Success !',
                            type: 'error',
                            buttons: {
                                confirm: {
                                    text: 'OK',
                                    className: 'btn btn-danger'
                                }
                            }
                        }).then((Delete) => {
                            if (Delete) {
                                window.location.replace("list-quotation.php");
                            } else {
                                window.location.replace("list-quotation.php");
                            }
                        });
                    };

                    return {
                        init: function() {
                            initDemos();
                        },
                    };
                }();

                jQuery(document).ready(function() {
                    SweetAlert2Demo.init();
                });

                </script>
                <?php } ?>
            </div>
            <!-- footer -->
            <?php include('footer.php');?>
            <!-- End footer -->
        </div>

        <!-- Custom template | don't include it in your project! -->
        <?php include('rightSidebar.php');?>
        <!-- End Custom template -->
    </div>

    <script src="assets/js/core/popper.min.js"></script>
    <script src="assets/js/core/bootstrap.min.js"></script>
    <!-- jQuery UI -->
    <script src="assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <script src="assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

    <!-- jQuery Scrollbar -->
    <script src="assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <!-- Atlantis JS -->
    <script src="assets/js/atlantis.min.js"></script>
    <!-- Atlantis DEMO methods, don't include it in your project! -->
    <script src="assets/js/setting-demo2.js"></script>
    <!-- Sweet Alert -->
    <script src="assets/js/plugin/sweetalert/sweetalert.min.js"></script>
</body>

</html>
