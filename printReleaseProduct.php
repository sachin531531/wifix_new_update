<!DOCTYPE html>
<html lang="en" id="printElement">

<?php 
session_start();

	if (!isset($_SESSION['user_name'])){
		header('Location: login.php?err=1');
	}
?>

<?php include 'db/dbConnection.php'; ?>

<?php
// $id = $_GET['id'];
$sql = mysqli_query($connection, "SELECT * FROM wefix_soft.pro_releas_tbl order by releas_id desc");
$res = mysqli_fetch_array($sql);
// var_dump($res["releas_id"]);
?>

<head>
    <meta charset="utf-8">
    <title>Invoice | WEFIX</title>
    <link rel="icon" href="assets/img/icon.ico" type="image/x-icon" />
    <script src="assets/js/plugin/webfont/webfont.min.js"></script>
    <script>
    WebFont.load({
        google: {
            "families": ["Lato:300,400,700,900"]
        },
        custom: {
            "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands",
                "simple-line-icons"
            ],
            urls: ['assets/css/fonts.min.css']
        },
        active: function() {
            sessionStorage.fonts = true;
        }
    });
    </script>
    <link rel="stylesheet" href="assets/css/invoiceStyle.css" media="all" />
    <link rel="stylesheet" href="assets/css/atlantis.min.css">
    <style>
    /* override styles when printing */
    @media print {
        #openWin {
            display: none;
        }

        #backbtn {
            display: none;
        }
    }
    </style>
</head>

<button type="button" id="openWin" class="btn btn-icon btn-round" title=""
    style="position:fixed;margin:auto; bottom:250px; right:10px; width:60px; height:60px; background-color:#5C55BF; border:none; cursor:pointer;"
    onclick="window.print();return false;">
    <i class="fas fa-print" style="font-size:180%; color:white;"></i>
</button>
<button type="button" class="btn btn-icon btn-round" id="backbtn"
    style="position:fixed;margin:auto; bottom:160px; right:10px; width:60px; height:60px; background-color:#5C55BF; border:none; cursor:pointer;"
    onclick="goBack()">
    <i class="fas fa-arrow-left" style="font-size:200%; color:white;"></i>
</button>

<body  style="background-color: white;">

 
    <?php
    // $total = 0;

    $sql = mysqli_query($connection, "SELECT * FROM wefix_soft.pro_releas_tbl order by releas_id desc limit 1");
    $res = mysqli_fetch_array($sql);

    $releas_id = $res['releas_id'];
    $releas_quot_id = $res['releas_quot_id'];
    $release_quot_det_id = $res['release_quot_det_id'];
    $releas_pro_name = $res['releas_pro_name'];
    $pro_releas_qty = $res['pro_releas_qty'];
    $releas_date = $res['releas_date'];
    $releas_user = $res['releas_user'];

    ?>
<!-- 
    <?php
    $sql1 = mysqli_query($connection, "SELECT * FROM invo_quotesetting_tbl WHERE id = 1");
    $res1 = mysqli_fetch_array($sql1);
    
    $name = $res1['name'];
    $number = $res1['number'];
    $email = $res1['email'];
    $image = $res1['image'];
    $address = $res1['address'];
    $footer = $res1['inFooter'];
    ?> -->

    <header class="clearfix"  style="background-color: white;">
        <div id="logo">
            <img src="image/<?php echo $image ?>">
        </div>
        <div id="company">
            <h2 class="name"><?php echo $name ?></h2>
            <div><?php echo $address ?></div>
            <div><?php echo $number ?></div>
            <div><a href="mailto:<?php echo $email ?>" target="_new"><?php echo $email ?></a></div>
        </div>
        </div>
    </header>
    <main style="background-color: white;">
        <div id="details" class="clearfix">
            <div id="client">
                <!-- <div class="to">INVOICE TO:</div> -->
                <!-- <h2 class="name"><?php echo $customer ?></h2> -->
                <!-- <div class="address"><?php echo $custAddress ?></div> -->
                <!-- <div class="email"><a href="mailto:<?php echo $custEmail ?>" target="_new"><?php echo $custEmail ?></a></div> -->
            </div>
            <div id="invoice">
                <h1>RELEASE PRODUCT</h1>
                <div class="date">Date: <?php echo $releas_date ?></div>
                <div class="date">Release Number: <?php echo $releas_id ?></div>
                <!-- <div class="date">Valid Date: 30/06/2014</div> -->
            </div>
        </div>
        <table border="0" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th class="unit" style="border-bottom: 0.5px solid #AAAAAA;">RELEASE QUOTATION ID</th>
                    <th class="desc" style="border-bottom: 0.5px solid #AAAAAA;">PRODUCT NAME</th>
                    <th class="unit" style="border-bottom: 0.5px solid #AAAAAA;">RELEASE QTY</th>
                    <th class="qty" style="border-bottom: 0.5px solid #AAAAAA;">RELEASE DATE</th>
                    <th class="unit" style="border-bottom: 0.5px solid #AAAAAA;">RELEASE USER</th>
                   
                </tr>
            </thead>
            <?php
             $sql="SELECT * FROM wefix_soft.pro_releas_tbl order by releas_id desc limit 1";
             $result = mysqli_query($connection,$sql);
             while($dataRow=mysqli_fetch_assoc($result)){ 
                $sql1="SELECT * FROM wefix_soft.user_tbl WHERE user_id=".$dataRow["releas_user"]."";
                $result1 = mysqli_query($connection,$sql1);
                while($dataRow1=mysqli_fetch_assoc($result1)){ 
                       $userNames = $dataRow1["user_name"];
                }
             }
            ?>
            <tbody>

              <?php
                    $sql="SELECT * FROM wefix_soft.pro_releas_tbl order by releas_id desc limit 1";
                    $result = mysqli_query($connection,$sql);
			        while($dataRow=mysqli_fetch_assoc($result)){ 
                ?> 

                <tr>
                    <td class="no"><?php echo $dataRow["releas_id"]; ?></td>
                    <td class="unit"> <?php echo $dataRow["releas_pro_name"]; ?> </td>
                    <td class="qty"><?php echo $dataRow["pro_releas_qty"]; ?></td>
                    <td class="unitPrice"><?php echo $dataRow["releas_date"]; ?> </td>
                    <td class="total"><?php echo $userNames; ?>
                    </td>
                </tr>
                <?php
        ; 
                    } 
                ?>


            </tbody>
            <?php
            $totWithDis = 0;
            $totDisc = 0;
            $totTax = 0;
            $totWithTax = 0;

            $sql = mysqli_query($connection, "SELECT * FROM tax_tbl WHERE id = '1'");
            $res = mysqli_fetch_array($sql);
            $tax = $res['tax'];
            ?>

        </table>
        <div id="thanks"><?php echo $footer ?></div>
    </main>

    <!--   Core JS Files   -->
    <script src="assets/js/core/jquery.3.2.1.min.js"></script>
    <!-- <script src="assets/js/atlantis.min.js"></script> -->
    <script>
    function goBack() {
        window.location = 'release-Products.php';
    }
    </script>
</body>

</html>