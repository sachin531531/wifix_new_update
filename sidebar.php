<?php include 'db/dbConnection.php'; ?>

<div class="sidebar sidebar-style-2">
    <style>
    .badge {
        padding: 3px 5px !important;
        font-size: 10px !important;
    }
    </style>
    <div class="sidebar-wrapper scrollbar scrollbar-inner">
        <div class="sidebar-content">
            <div class="user">
                <div class="avatar-sm float-left mr-2">
                    <img src="assets/img/profile.jpg" alt="..." class="avatar-img rounded-circle">
                </div>
                <div class="info">
                    <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
                        <span>
                            <?php echo $_SESSION['user_name'] ?>
                            <span class="user-level"><?php echo $_SESSION['user_type'] ?></span>
                            <span class="caret"></span>
                        </span>
                    </a>
                    <div class="clearfix"></div>

                    <?php if ($_SESSION['user1'] == 1) { ?>
                        <div class="collapse in" id="collapseExample">
                            <ul class="nav">
					    		<li>
					    			<a href="list-users.php">
					    				<span class="sub-item">List Users</span>
					    			</a>
					    		</li>
                            </ul>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <ul class="nav nav-primary">
                <li class="nav-item active">
                    <a href="index.php">
                        <i class="fas fa-home" id="dash"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="nav-section">
                    <span class="sidebar-mini-icon">
                        <i class="fa fa-ellipsis-h"></i>
                    </span>
                    <h4 class="text-section">Components</h4>
                </li>

                <li class="nav-item">
                    <a data-toggle="collapse" href="#Products">
                        <i class="fas fa-briefcase"></i>
                        <p>Job List</p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse" id="Products">
                        <ul class="nav nav-collapse">
                        
                            <li>
                                <a <?php if ($_SESSION['jobCat'] == 1) { ?>href="list-jobCategory.php"<?php } ?> style="cursor:pointer;">
                                    <span class="sub-item">List Job Category <?php if ($_SESSION['jobCat'] == 0) { ?><span class="badge badge-danger">X</span><?php } ?></span>
                                </a>
                            </li>
                            <li>
                                <a <?php if ($_SESSION['job'] == 1) { ?>href="list-job.php"<?php } ?> style="cursor:pointer;">
                                    <span class="sub-item">List Job <?php if ($_SESSION['job'] == 0) { ?><span class="badge badge-danger">X</span><?php } ?></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="nav-item">
                    <a data-toggle="collapse" href="#customers">
                        <i class="fas fa-user-friends"></i>
                        <p>Customers</p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse" id="customers">
                        <ul class="nav nav-collapse">
                            <li>
                                <a <?php if ($_SESSION['customer'] == 1) { ?>href="list-customers.php"<?php } ?> style="cursor:pointer;">
                                    <span class="sub-item">List Customers <?php if ($_SESSION['customer'] == 0) { ?><span class="badge badge-danger">X</span><?php } ?></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a data-toggle="collapse" href="#Customers">
                        <i class="fas fa-users"></i>
                        <p>Labour Packs</p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse" id="Customers">
                        <ul class="nav nav-collapse">
                            <li>
                                <a <?php if ($_SESSION['labour'] == 1) { ?>href="list-labours.php"<?php } ?> style="cursor:pointer;">
                                    <span class="sub-item">List Labours <?php if ($_SESSION['labour'] == 0) { ?><span class="badge badge-danger">X</span><?php } ?></span>
                                </a>
                            </li>
                            <li>
                                <a <?php if ($_SESSION['labourPack'] == 1) { ?>href="list-labour-pack.php"<?php } ?> style="cursor:pointer;">
                                    <span class="sub-item">Labour Packs <?php if ($_SESSION['labourPack'] == 0) { ?><span class="badge badge-danger">X</span><?php } ?></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="nav-item">
                    <a data-toggle="collapse" href="#Suppliers">
                        <i class="fas fa-shipping-fast"></i>
                        <p>products</p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse" id="Suppliers">
                        <ul class="nav nav-collapse">
                            <li>
                                <a <?php if ($_SESSION['category'] == 1) { ?>href="list-categories.php"<?php } ?> style="cursor:pointer;">
                                    <span class="sub-item">List Categories <?php if ($_SESSION['category'] == 0) { ?><span class="badge badge-danger">X</span><?php } ?></span>
                                </a>
                            </li>
                            <li>
                                <a <?php if ($_SESSION['subCate'] == 1) { ?>href="list-subCategories.php"<?php } ?> style="cursor:pointer;">
                                    <span class="sub-item">List Subcategories <?php if ($_SESSION['subCate'] == 0) { ?><span class="badge badge-danger">X</span><?php } ?></span>
                                </a>
                            </li>
                            <li>
                                <a <?php if ($_SESSION['brand'] == 1) { ?>href="list-brands.php"<?php } ?> style="cursor:pointer;">
                                    <span class="sub-item">List Brand <?php if ($_SESSION['brand'] == 0) { ?><span class="badge badge-danger">X</span><?php } ?></span>
                                </a>
                            </li>
                            <li>
                                <a <?php if ($_SESSION['products'] == 1) { ?>href="list-products.php"<?php } ?> style="cursor:pointer;">
                                    <span class="sub-item">List Products <?php if ($_SESSION['products'] == 0) { ?><span class="badge badge-danger">X</span><?php } ?></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="nav-item">
                    <a data-toggle="collapse" href="#purchese">
                        <i class="fas fa-truck-loading"></i>
                        <p>Suppliers</p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse" id="purchese">
                        <ul class="nav nav-collapse">
                            <li>
                                <a <?php if ($_SESSION['suppliers'] == 1) { ?>href="list-supplier.php"<?php } ?> style="cursor:pointer;">
                                    <span class="sub-item">List Suppliers <?php if ($_SESSION['suppliers'] == 0) { ?><span class="badge badge-danger">X</span><?php } ?></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="nav-item">
                    <a data-toggle="collapse" href="#Locations">
                        <i class="fas fa-file-invoice"></i>
                        <p>Quotation/Invoice</p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse" id="Locations">
                        <ul class="nav nav-collapse">
                            <li>
                                <a <?php if ($_SESSION['quote'] == 1) { ?>href="list-quotation.php"<?php } ?> style="cursor:pointer;">
                                    <span class="sub-item">List Quotations <?php if ($_SESSION['quote'] == 0) { ?><span class="badge badge-danger">X</span><?php } ?></span>
                                </a>
                            </li>
                            <li>
                                <a <?php if ($_SESSION['reQuote'] == 1) { ?>href="list_revise_quote.php"<?php } ?> style="cursor:pointer;">
                                    <span class="sub-item">List Revise Quotations <?php if ($_SESSION['reQuote'] == 0) { ?><span class="badge badge-danger">X</span><?php } ?></span>
                                </a>
                            </li>
                            <li>
                                <a <?php if ($_SESSION['invoice'] == 1) { ?>href="list-invoice.php"<?php } ?> style="cursor:pointer;">
                                    <span class="sub-item">List Invoices <?php if ($_SESSION['invoice'] == 0) { ?><span class="badge badge-danger">X</span><?php } ?></span>
                                </a>
                            </li>
                            <li>
                                <a <?php if ($_SESSION['reInvoice'] == 1) { ?>href="list_revise_invoice.php"<?php } ?> style="cursor:pointer;">
                                    <span class="sub-item">List Revise Invoices <?php if ($_SESSION['reInvoice'] == 0) { ?><span class="badge badge-danger">X</span><?php } ?></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="nav-item">
                    <a data-toggle="collapse" href="#stock">
                        <i class="fas fa-dolly-flatbed"></i>
                        <p>Stock</p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse" id="stock">
                        <ul class="nav nav-collapse">
                            <li>
                                <a <?php if ($_SESSION['grn'] == 1) { ?>href="good-receive-note.php"<?php } ?> style="cursor:pointer;">
                                    <span class="sub-item">Good Receive Notes <?php if ($_SESSION['grn'] == 0) { ?><span class="badge badge-danger">X</span><?php } ?></span>
                                </a>
                            </li>
                            <li>
                                <a <?php if ($_SESSION['release1'] == 1) { ?>href="release-Products.php"<?php } ?> style="cursor:pointer;">
                                    <span class="sub-item">Release Products <?php if ($_SESSION['release1'] == 0) { ?><span class="badge badge-danger">X</span><?php } ?></span>
                                </a>
                            </li>
                            <li>
                                <a <?php if ($_SESSION['maReturn'] == 1) { ?>href="matirial-return.php"<?php } ?> style="cursor:pointer;">
                                    <span class="sub-item">Material Return <?php if ($_SESSION['maReturn'] == 0) { ?><span class="badge badge-danger">X</span><?php } ?></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a data-toggle="collapse" href="#Reports">
                        <i class="fas fa-signal"></i>
                        <p>Reports</p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse" id="Reports">
                        <ul class="nav nav-collapse">

                        <!-- <li>
                                <a href="job-report.php">
                                    <span class="sub-item">Job report</span>
                                </a>
                            </li> -->
                            <li>
                                <a <?php if ($_SESSION['stockRepo'] == 1) { ?>href="stock-report.php"<?php } ?> style="cursor:pointer;">
                                    <span class="sub-item">Stock Reports <?php if ($_SESSION['stockRepo'] == 0) { ?><span class="badge badge-danger">X</span><?php } ?></span>
                                </a>
                            </li>
                            <li>
                                <a <?php if ($_SESSION['reProReport'] == 1) { ?>href="reorder-product.php"<?php } ?> style="cursor:pointer;">
                                    <span class="sub-item">Reorder Pro. Reports <?php if ($_SESSION['reProReport'] == 0) { ?><span class="badge badge-danger">X</span><?php } ?></span>
                                </a>
                            </li>
                            <li>
                                <a <?php if ($_SESSION['matReReport'] == 1) { ?>href="material-report.php"<?php } ?> style="cursor:pointer;">
                                    <span class="sub-item">Material Return Reports <?php if ($_SESSION['matReReport'] == 0) { ?><span class="badge badge-danger">X</span><?php } ?></span>
                                </a>
                            </li>
                            <li>
                                <a <?php if ($_SESSION['proReport'] == 1) { ?>href="product-report.php"<?php } ?> style="cursor:pointer;">
                                    <span class="sub-item">Product report <?php if ($_SESSION['proReport'] == 0) { ?><span class="badge badge-danger">X</span><?php } ?></span>
                                </a>
                            </li>
                            <li>
                                <a href="outstanding-reports.php">
                                    <span class="sub-item">Outstanding Reports</span>
                                </a>
                            </li>

                            <li>
                                <a <?php if ($_SESSION['quoteReport'] == 1) { ?>href="quotation-report.php"<?php } ?> style="cursor:pointer;">
                                    <span class="sub-item">Quotation Reports <?php if ($_SESSION['quoteReport'] == 0) { ?><span class="badge badge-danger">X</span><?php } ?></span>
                                </a>
                            </li>
                            <li>
                                <a <?php if ($_SESSION['invReport'] == 1) { ?>href="invoice-report.php"<?php } ?> style="cursor:pointer;">
                                    <span class="sub-item">Invoice Reports <?php if ($_SESSION['invReport'] == 0) { ?><span class="badge badge-danger">X</span><?php } ?></span>
                                </a>
                            </li>
                            <li>
                                <a <?php if ($_SESSION['payReport'] == 1) { ?>href="payment-report.php"<?php } ?> style="cursor:pointer;">
                                    <span class="sub-item">Payment Reports <?php if ($_SESSION['payReport'] == 0) { ?><span class="badge badge-danger">X</span><?php } ?></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="nav-item">
                    <a data-toggle="collapse" href="#setting">
                        <i class="fas fa-cogs"></i>
                        <p>setting</p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse" id="setting">
                        <ul class="nav nav-collapse">
                            <li>
                                <a <?php if ($_SESSION['invSett'] == 1) { ?>href="invoice-setting.php"<?php } ?> style="cursor:pointer;">
                                    <span class="sub-item">Invoice/Quotation Setting <?php if ($_SESSION['invSett'] == 0) { ?><span class="badge badge-danger">X</span><?php } ?></span>
                                </a>
                            </li>
                            <li>
                                <a <?php if ($_SESSION['tax'] == 1) { ?>href="tax-setting.php"<?php } ?> style="cursor:pointer;">
                                    <span class="sub-item">Tax Setting <?php if ($_SESSION['tax'] == 0) { ?><span class="badge badge-danger">X</span><?php } ?></span>
                                </a>
                            </li>
                            <li>
                                <a <?php if ($_SESSION['email'] == 1) { ?>href="list-email.php"<?php } ?> style="cursor:pointer;">
                                    <span class="sub-item">Quotation Email <?php if ($_SESSION['email'] == 0) { ?><span class="badge badge-danger">X</span><?php } ?></span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="mx-4 mt-2">
                    <a href="logout.php" class="btn btn-primary btn-block"><span class="btn-label mr-2"> <i class="fas fa-sign-out-alt"></i> </span>Logout</a>
                </li>
            </ul>
        </div>
    </div>
</div>