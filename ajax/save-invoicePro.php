<?php
    include('../db/dbConnection.php');
?>
<?php
    session_start();

    $data = (array) $_POST['data'];

    $jobId = $data['jobId'];
    $disc = $data['disc'];
    $cusId = $data['cusId'];

    $transporation_cost=$data['transporationCost'];
    $over_head_cost=$data['overHeadCost'];
    $conditions=$data['conditions'];
    $profit_margines=$data['profitMargines'];


    $date = date("m/d/Y");
    date_default_timezone_set("Asia/Colombo");
    $time = date("h:i:sa");
    $user = $_SESSION['user_id'];

    $select="SELECT MAX(invoice_id) AS max_id FROM invoice_tbl";
    $result= mysqli_query($connection,$select);
    $dataRow=mysqli_fetch_array($result);
    $dataRow = ++$dataRow['max_id'];

    $sql = mysqli_query($connection, "SELECT * FROM job_tbl,jobcategory_tbl WHERE job_tbl.job_id = '$jobId' AND jobcategory_tbl.jobCat_name = job_tbl.service_type");
    $res = mysqli_fetch_array($sql);

    $jobRef = $res['jobCat_subName'];

    $year = date("Y");
    $invNo = "WEFIX/IN/".$year."/".$jobRef."/".$dataRow;

    mysqli_autocommit($connection, false);

                    $query1 = "INSERT INTO invoice_tbl(`invoice_id`,`invoice_job`,`added_user`,`added_date`,`added_time`,`revise_invoice`,`reInvoice`,`invoice_no`,`quoteRef_id`,`invCus_id`,`inv_disc`,`payment`,`add_tax`,`transporation_cost`,`over_head_cost`,`conditions`,`profit_margines`)VALUES(
                        0,
                        $jobId,
                        $user,
                        '$date',
                        '$time',
                        0,
                        0,
                        '$invNo',
                        $dataRow,
                        $cusId,
                        $disc,
                        0,
                        0,
                        $transporation_cost,
                        $over_head_cost,
                        '$conditions',
                        $profit_margines)";

                    $result1 = mysqli_query($connection, $query1);
                    $response_array['----->>'] =$query1;
                    if ($result1) {
                        if (!empty($data['proObj'])) {

                            for ($x = 0; $x < count($data['proObj']); $x++) {

                                $stockId = $data['proObj'][$x]['id'];
                                $proCode = $data['proObj'][$x]['code'];
                                $proName = $data['proObj'][$x]['proName'];
                                $price = $data['proObj'][$x]['price'];
                                $totQty = $data['proObj'][$x]['totQty'];
                                $description = $data['proObj'][$x]['desc'];

                                $query2 = "INSERT INTO invoice_details_tbl(`invoice_det_id`,`invoice_id`,`stock_id`,`pro_code`,`pro_name`,`totQty`,`invoice_desc`,`invoice_price`)VALUES(
                                    0,
                                    $dataRow,
                                    $stockId,
                                    '$proCode',
                                    '$proName',
                                    $totQty,
                                    '$description',
                                    $price)";

                                $result2 = mysqli_query($connection, $query2);
                                $response_array['data'] = $result2;
                                if (!$result2) {
                                    mysqli_rollback($connection);
                                    $response_array['status'] = 'error';
                                    echo json_encode($response_array);
                                }
                            }
                        }else {
                            mysqli_rollback($connection);
                            $response_array['status'] = 'error';
                            echo json_encode($response_array);
                        }

                        if (!empty($data['labourArray'])){
                            for ($x = 0; $x < count($data['labourArray']); $x++) {
                                $packId = $data['labourArray'][$x]['ID'];
                                $query4 = "INSERT INTO invoice_lapack_tbl(`invLab_id`,`invPack_id`,`invoiceRef_id`)VALUES(
                                    0,
                                    $packId,
                                    $dataRow)";

                                $result4 = mysqli_query($connection, $query4);

                                if (!$result4) {
                                    mysqli_rollback($connection);
                                    $response_array['status'] = 'error';
                                    echo json_encode($response_array);
                                }
                            }
                        }
                        $query6 = "UPDATE job_tbl SET 
                                        send_invoice='1',
                                        job_status='Send Invoice'
                                        WHERE 
                                        job_id='$jobId'";

                        $result6 = mysqli_query($connection, $query6);

                        if (!$result6) {
                            mysqli_rollback($connection);
                            $response_array['status'] = 'error';
                            echo json_encode($response_array);
                        }else {
                            if (!empty($data['additionalArray'])) {
                            for ($x = 0; $x < count($data['additionalArray']); $x++) {

                                $mainDesc = $data['additionalArray'][$x]['mainDesc'];
                                $subDesc = $data['additionalArray'][$x]['subDesc'];
                                $price = $data['additionalArray'][$x]['price'];

                                $query7 = "INSERT INTO invoice_additional_tbl(`additional_id`,`mainDec`,`subDesc`,`additional_price`,`invoice_id`)VALUES(
                                    0,
                                    '$mainDesc',
                                    '$subDesc',
                                    $price,
                                    $dataRow)";

                                $result7 = mysqli_query($connection, $query7);

                                if (!$result7) {
                                    mysqli_rollback($connection);
                                    $response_array['status'] = 'error';
                                    echo json_encode($response_array);
                                }
                            }
                        }
                        }

                        mysqli_commit($connection);
                        $response_array['pos_id'] = $dataRow;
                        $response_array['status'] = 'success';
                        echo json_encode($response_array);

                    }else {
                        mysqli_rollback($connection);
                        $response_array['status'] = 'error';
                        echo json_encode($response_array);
                    }
                

?>
