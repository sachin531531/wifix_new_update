<?php
include('../db/dbConnection.php');
?>

<?php
	$sql = mysqli_query($connection,"SELECT * FROM stock_tbl,products_tbl WHERE products_tbl.pro_id = stock_tbl.pro_id");
	$row = mysqli_num_rows($sql);
	while ($row = mysqli_fetch_array($sql)){
        $id = $row['stock_id'];
        echo "<div class='col-4 '>
            <div class='card card-post card-round' style='background-color:#48AAF7; cursor:pointer;' onclick='viewDetail(".$id.")'>
                <img class='card-img-top' src='assets/img/productimg/".$row['pro_img']."'>
                <p style='text-align:center; color:white; margin-top:0px; margin-bottom:0px;'>".$row['pro_name']."</p>
                <p style='text-align:center; color:white; margin-top:0px; margin-bottom:0px;'>qty : ".$row['stock_qty']."</p>
            </div>
        </div>";
	}
?>