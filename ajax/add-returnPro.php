<?php
include('../db/dbConnection.php');

$data = (array) $_POST['data'];

    $invId = $data['invId'];
    $invNo = $data['invNo'];
?>

<?php
session_start();
$date = date("m/d/Y");
$user = $_SESSION['user_id'];
$stack = array();

$notSend = true;
$notUpdate = true;

mysqli_autocommit($connection, false);

for ($x = 0; $x < count($data['proObj']); $x++) {

    $id = $data['proObj'][$x]['id'];
    $code = $data['proObj'][$x]['code'];
    $proName = $data['proObj'][$x]['proName'];
    $price = $data['proObj'][$x]['price'];
    $qty = $data['proObj'][$x]['qty'];
    $totQty = $data['proObj'][$x]['totQty'];
    $desc = $data['proObj'][$x]['desc'];

    $sql1 = mysqli_query($connection, "SELECT * FROM stock_tbl WHERE stock_id = '$code'");
    $res = mysqli_fetch_array($sql1);

    $stockQTY = $res['stock_qty'];
    $totalQTY = $stockQTY + $totQty;

    $query1 = "UPDATE stock_tbl SET stock_qty='$totalQTY' WHERE stock_id='$code'";
    $result1 = mysqli_query($connection, $query1);

    if (!$result1) {
        mysqli_rollback($connection);
        $response_array['status'] = 'error';
        echo json_encode($response_array);
        $notUpdate = false;
        break;
    }else{
        $query5 = "INSERT INTO returnmaterial_tbl(`id`,`re_stock_id`,`re_invoice_id`,`re_qty`,`re_date`)VALUES(
            '',
            '$code',
            '$invId',
            '$totQty',
            '$date')";
    
        $result5 = mysqli_query($connection, $query5);

        if (!$result5) {
            mysqli_rollback($connection);
            $response_array['status'] = 'error';
            echo json_encode($response_array);
            break;
        }
    }
}

mysqli_commit($connection);
$response_array['status'] = 'success';
echo json_encode($response_array);

?>