<?php
    include('../db/dbConnection.php');
?>
<?php
    session_start();

    $data = (array) $_POST['data'];

    $name = $mysqli->escape_string($data['name']);
    $pwd =$mysqli->escape_string(password_hash($data['pwd'], PASSWORD_BCRYPT));
    $role = $data['role'];
    $user = $data['user'];
    $customer = $data['customer'];
    $suppliers = $data['suppliers'];
    $jobCat = $data['jobCat'];
    $job = $data['job'];
    $labour = $data['labour'];
    $labourPack = $data['labourPack'];
    $grn = $data['grn'];
    $release = $data['release'];
    $maReturn = $data['maReturn'];
    $invSett = $data['invSett'];
    $tax = $data['tax'];
    $email = $data['email'];
    $category = $data['category'];
    $subCate = $data['subCate'];
    $brand = $data['brand'];
    $products = $data['products'];
    $quote = $data['quote'];
    $reQuote = $data['reQuote'];
    $invoice = $data['invoice'];
    $reInvoice = $data['reInvoice'];
    $stockRepo = $data['stockRepo'];
    $reProReport = $data['reProReport'];
    $matReReport = $data['matReReport'];
    $proReport = $data['proReport'];
    $quoteReport = $data['quoteReport'];
    $invReport = $data['invReport'];
    $payReport = $data['payReport'];


    $date = date("m/d/Y");
    $user = $_SESSION['user_id'];

    mysqli_autocommit($connection, false);
    $result = $mysqli->query("SELECT * FROM user_tbl WHERE user_name='$name'") or die($mysqli->error());

    if ( $result->num_rows > 0 ) { 
      $response_array['status'] = 'error';
        echo json_encode($response_array);
    }else{
    $query1 = "INSERT INTO user_tbl(`user_name`,`user_pwd`,`user_type`,`user1`,`customer`,`suppliers`,`jobCat`,`job`,`labour`,`labourPack`,`grn`,`release1`,`maReturn`,`invSett`,`tax`,`email`,`category`,`subCate`,`brand`,`products`,`quote`,`reQuote`,`invoice`,`reInvoice`,`stockRepo`,`reProReport`,`matReReport`,`proReport`,`quoteReport`,`invReport`,`payReport`,`added_user`,`added_date`)VALUES(
                
                '$name',
                '$pwd',
                '$role',
                '$user',
                '$customer',
                '$suppliers',
                '$jobCat',
                '$job',
                '$labour',
                '$labourPack',
                '$grn',
                '$release',
                '$maReturn',
                '$invSett',
                '$tax',
                '$email',
                '$category',
                '$subCate',
                '$brand',
                '$products',
                '$quote',
                '$reQuote',
                '$invoice',
                '$reInvoice',
                '$stockRepo',
                '$reProReport',
                '$matReReport',
                '$proReport',
                '$quoteReport',
                '$invReport',
                '$payReport',
                '$user',
                '$date')";

    $result1 = mysqli_query($connection, $query1);

    if ($result1) {
        mysqli_commit($connection);
        $response_array['status'] = 'success';
        echo json_encode($response_array);
    }else {
        mysqli_rollback($connection);
        $response_array['status'] = 'error';
        echo json_encode($response_array);
    }
}
?>