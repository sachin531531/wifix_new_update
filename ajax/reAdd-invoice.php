<?php
    include('../db/dbConnection.php');
?>
<?php
    session_start();
 
    $data = (array) $_POST['data'];

    // var_dump($data);

    $jobId = $data['jobId'];
    $editQuoteId = $data['quoteId'];
    $quoteRef_id = $data['quoteRef'];
    $quotNo = substr_replace($data['quoteNo'], 'R/', 9, 0);
    $cusId = $data['cusId'];
    $disc = $data['disc'];

    $transporation_cost=$data['transporationCost'];
    $over_head_cost=$data['overHeadCost'];
    $conditions=$data['conditions'];
    $profit_margines=$data['profitMargines'];


    $date = date("m/d/Y");
    date_default_timezone_set("Asia/Colombo");
    $time = date("h:i:sa");
    $user = $_SESSION['user_id'];

    $addTax = $data['addTax'];

    $select="SELECT MAX(invoice_id) AS max_id FROM invoice_tbl";
    $result= mysqli_query($connection,$select);
    $dataRow=mysqli_fetch_array($result);
    $dataRow = ++$dataRow['max_id'];

    mysqli_autocommit($connection, false);

                    $query1 = "INSERT INTO invoice_tbl(`invoice_id`,`invoice_job`,`added_user`,`added_date`,`added_time`,`revise_invoice`,`reInvoice`,`invoice_no`,`quoteRef_id`,`invCus_id`,`inv_disc`,`payment`,`add_tax`,`transporation_cost`,`over_head_cost`,`conditions`,`profit_margines`)VALUES(
                        '$dataRow',
                        '$jobId',
                        '$user',
                        '$date',
                        '$time',
                        '0',
                        '1',
                        '$quotNo',
                        '$quoteRef_id',
                        '$cusId',
                        '$disc',
                        '0',
                        '$addTax',
                        $transporation_cost,
                        $over_head_cost,
                        '$conditions',
                        $profit_margines
                        )";

                    $result1 = mysqli_query($connection, $query1);

                    if ($result1) {
                        if (!empty($data['proObj'])) {

                            for ($x = 0; $x < count($data['proObj']); $x++) {

                                $stockId = $data['proObj'][$x]['id'];
                                $proCode = $data['proObj'][$x]['code'];
                                $proName = $data['proObj'][$x]['proName'];
                                $price = $data['proObj'][$x]['price'];
                                $totQty = $data['proObj'][$x]['totQty'];
                                $description = $data['proObj'][$x]['desc'];

                                $query2 = "INSERT INTO invoice_details_tbl(`invoice_det_id`,`invoice_id`,`stock_id`,`pro_code`,`pro_name`,`totQty`,`invoice_desc`,`invoice_price`)VALUES(
                                    0,
                                    '$dataRow',
                                    '$stockId',
                                    '$proCode',
                                    '$proName',
                                    '$totQty',
                                    '$description',
                                    '$price')";

                                $result2 = mysqli_query($connection, $query2);
                                if (!$result2) {
                                    mysqli_rollback($connection);
                                    $response_array['status'] = 'error';
                                    echo json_encode($response_array);
                                    // echo "1";
                                    // breack;
                                }
                            }
                        }else {
                            mysqli_rollback($connection);
                            $response_array['status'] = 'error';
                            echo json_encode($response_array);
                            echo "2";
                        }
                        if(!empty($data['labourArray'])) {
                            for ($x = 0; $x < count($data['labourArray']); $x++) {

                                $packId = $data['labourArray'][$x]['ID'];

                                $query4 = "INSERT INTO invoice_lapack_tbl(`invLab_id`,`invPack_id`,`invoiceRef_id`)VALUES(
                                '',
                                '$packId',
                                '$dataRow')";

                                $result4 = mysqli_query($connection, $query4);

                                if (!$result4) {
                                    mysqli_rollback($connection);
                                    $response_array['status'] = 'error';
                                    echo json_encode($response_array);
                                    echo "3";
                                }
                            }
                        }

                        $query6 = "UPDATE job_tbl SET 
                                        send_invoice='1',
                                        job_status='Send Invoice'
                                        WHERE 
                                        job_id='$jobId'";

                        $result6 = mysqli_query($connection, $query6);

                        if (!$result6) {
                            mysqli_rollback($connection);
                            $response_array['status'] = 'error';
                            echo json_encode($response_array);
                            echo "4";
                        }else {
                            if (!empty($data['additionalArray'])) {
                            for ($x = 0; $x < count($data['additionalArray']); $x++) {

                                $mainDesc = $data['additionalArray'][$x]['mainDesc'];
                                $subDesc = $data['additionalArray'][$x]['subDesc'];
                                $price = $data['additionalArray'][$x]['price'];

                                $query7 = "INSERT INTO invoice_additional_tbl(`additional_id`,`mainDec`,`subDesc`,`additional_price`,`invoice_id`)VALUES(
                                    '',
                                    '$mainDesc',
                                    '$subDesc',
                                    '$price',
                                    '$dataRow')";

                                $result7 = mysqli_query($connection, $query7);

                                if (!$result7) {
                                    mysqli_rollback($connection);
                                    $response_array['status'] = 'error';
                                    echo json_encode($response_array);
                                    echo "5";
                                }
                            }
                        }
                        }

                        mysqli_commit($connection);
                        $response_array['pos_id'] = $dataRow;
                        $response_array['status'] = 'success';
                        echo json_encode($response_array);

                    }else {
                        mysqli_rollback($connection);
                        $response_array['status'] = 'error';
                        echo json_encode($response_array);
                        echo "6";
                    }
                

?>