<?php include 'db/dbConnection.php'; ?>

<?php
// $id = $_GET['id'];
$id = 5;


$sql = mysqli_query($connection, "SELECT * FROM invo_quotesetting_tbl WHERE id = 1");
$res = mysqli_fetch_array($sql);

$name = $res['name'];
$number = $res['number'];
$email = $res['email'];
$image = $res['image'];
$address = $res['address'];
$footer = $res['footer'];
$inFooter = $res['inFooter'];

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Invoice Setting | WEFIX</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <link rel="icon" href="assets/img/icon.ico" type="image/x-icon" />

    <!-- Fonts and icons -->
    <script src="assets/js/plugin/webfont/webfont.min.js"></script>
    <script>
    WebFont.load({
        google: {
            "families": ["Lato:300,400,700,900"]
        },
        custom: {
            "families": ["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands",
                "simple-line-icons"
            ],
            urls: ['assets/css/fonts.min.css']
        },
        active: function() {
            sessionStorage.fonts = true;
        }
    });
    </script>

    <style>
    .fileUpload {
        /* position: relative; */
        overflow: hidden;
        /* margin: 10px; */
    }

    .fileUpload input.upload {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }

    .lds-ellipsis {
        display: inline-block;
        position: relative;
        width: 64px;
        height: 64px;
    }

    .lds-ellipsis div {
        position: absolute;
        top: 27px;
        width: 11px;
        height: 11px;
        border-radius: 50%;
        background: #fff;
        animation-timing-function: cubic-bezier(0, 1, 1, 0);
    }

    .lds-ellipsis div:nth-child(1) {
        left: 6px;
        animation: lds-ellipsis1 0.6s infinite;
    }

    .lds-ellipsis div:nth-child(2) {
        left: 6px;
        animation: lds-ellipsis2 0.6s infinite;
    }

    .lds-ellipsis div:nth-child(3) {
        left: 26px;
        animation: lds-ellipsis2 0.6s infinite;
    }

    .lds-ellipsis div:nth-child(4) {
        left: 45px;
        animation: lds-ellipsis3 0.6s infinite;
    }

    @keyframes lds-ellipsis1 {
        0% {
            transform: scale(0);
        }

        100% {
            transform: scale(1);
        }
    }

    @keyframes lds-ellipsis3 {
        0% {
            transform: scale(1);
        }

        100% {
            transform: scale(0);
        }
    }

    @keyframes lds-ellipsis2 {
        0% {
            transform: translate(0, 0);
        }

        100% {
            transform: translate(19px, 0);
        }
    }
    .tox-statusbar__branding{
        display:none;
    }
    </style>

    <!-- CSS Files -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/atlantis.min.css">
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link rel="stylesheet" href="assets/css/demo.css">
</head>

<body>
    <div class="wrapper">
        <!-- Navbar Header -->
        <?php include('header.php');?>
        <!-- End Navbar -->
        <!-- Sidebar -->
        <?php include('sidebar.php');?>
        <!-- End Sidebar -->
        <div class="main-panel">
            <div class="content">
                <div class="page-inner">
                    <div class="page-header">
                        <h4 class="page-title">INVOICE, QUOTATION SETTING</h4>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <form action="edit_invoiceSetting.php" method="post" enctype="multipart/form-data">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="">Company Name</label>
                                                    <input type="text" class="form-control" id="" name="txt_name"
                                                        value="<?php echo $name; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Company Address</label>
                                                    <input type="" class="form-control" id="" name="txt_address"
                                                        value="<?php echo $address; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Phone Number</label>
                                                    <input type="text" class="form-control" id="" name="txt_phone"
                                                        value="<?php echo $number; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">email</label>
                                                    <input type="text" class="form-control" id="" name="txt_email"
                                                        value="<?php echo $email; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Quotation Footer</label>
                                                    <textarea class="form-control" id="" rows="5"
                                                        name="txt_footer"><?php echo $footer; ?></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Invoice Footer</label>
                                                    <textarea class="form-control" id="" rows="5"
                                                        name="txt_inFooter"><?php echo $inFooter; ?></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Logo (JPEG, JPG Format)</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="fileUpload btn btn-primary">
                                                                <span>Upload</span>
                                                                <input id="uploadBtn" type="file" class="upload"
                                                                    name="txt_img" />
                                                            </div>
                                                        </div>
                                                        <input type="text" id="uploadFile" class="form-control"
                                                            placeholder="Choose File" aria-label=""
                                                            aria-describedby="basic-addon1" disabled="disabled">
                                                    </div>
                                                    <script>
                                                    document.getElementById("uploadBtn").onchange = function() {
                                                        document.getElementById("uploadFile").value = this.value;
                                                    };
                                                    </script>
                                                </div>
                                                <div class="form-group">
                                                    <img src="image/<?php echo $image; ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-action">
                                        <button type="submit" class="btn btn-primary">
                                            <span class="btn-label">
                                                <i class="fa fa-check"></i>
                                            </span>
                                            Edit
                                        </button>

                                        <!-- <a href='list-customers.php' class="btn btn-danger">
                                            <span class="btn-label">
                                                <i class="fa fa-times"></i>
                                            </span>
                                            Cancel
                                        </a> -->
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer -->
            <?php include('footer.php');?>
            <!-- End footer -->
        </div>

        <!-- Custom template | don't include it in your project! -->
        <?php include('rightSidebar.php');?>
        <!-- End Custom template -->
    </div>
    <!--   Core JS Files   -->
    <script src="assets/js/core/jquery.3.2.1.min.js"></script>
    <script src="assets/js/core/popper.min.js"></script>
    <script src="assets/js/core/bootstrap.min.js"></script>
    <!-- jQuery UI -->
    <script src="assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
    <script src="assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

    <!-- jQuery Scrollbar -->
    <script src="assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <!-- Atlantis JS -->
    <script src="assets/js/atlantis.min.js"></script>
    <!-- Atlantis DEMO methods, don't include it in your project! -->
    <script src="assets/js/setting-demo2.js"></script>

    <!-- partial -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.0.5/tinymce.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script>
    <script>
    tinymce.init({
        selector: "textarea",
        plugins: "lists advlist autolink autoresize charmap code emoticons hr image insertdatetime link media paste preview searchreplace table textpattern toc visualblocks visualchars wordcount quickbars autoresize spellchecker",
        toolbar: "code preview | undo redo | formatselect | fontselect | fontsizeselect | bold italic underline strikethrough backcolor forecolor | subscript superscript | numlist bullist | alignleft aligncenter alignright alignjustify | outdent indent | paste searchreplace | toc link image media charmap insertdatetime emoticons hr | table tabledelete | tableprops tablerowprops tablecellprops | tableinsertrowbefore tableinsertrowafter tabledeleterow | tableinsertcolbefore tableinsertcolafter tabledeletecol | removeformat spellchecker",
        insertdatetime_element: true,
        media_scripts: [{
                filter: 'platform.twitter.com'
            },
            {
                filter: 's.imgur.com'
            },
            {
                filter: 'instagram.com'
            },
            {
                filter: 'https://platform.twitter.com/widgets.js'
            },
        ],
        browser_spellcheck: true,
        contextmenu: true,
        autoresize_on_init: true,
        max_height: 400,
        spellchecker_languages: 'English=en,Danish=da,Dutch=nl,Finnish=fi,French=fr_FR,' + 'German=de,Italian=it,Polish=pl,Portuguese=pt_BR,Spanish=es,Swedish=sv',
    });
    </script>
</body>

</html>